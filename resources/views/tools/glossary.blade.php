@extends('_layouts.app')

@section('keywords')
keywords
@stop
@section('description')
description
@stop


{{-- Web site Title --}}
@section('title')
Mortgage Glossary @parent
@stop

@section('styles')
@stop

@section('scripts')
@stop

@section('inline-scripts')
@stop


{{-- Content --}}
@section('content')


        <div style="margin-top: 95px;"></div>
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div class="about-title">
                <h1 class="text-center">
                  Glossary of Mortgage Terms
                </h1>
                <p class="lead text-center" style="margin-bottom: 80px">Since most of us don't use mortgage terminology in our normal daily conversations, listed below are some of the most commonly used terms. If you don't find a term below or still have questions, just ask us.</p>
              </div>
              <div class="about-body text-center">
                <div class="container">
                  <span id="glossary-nav">
                    <a href="#a">A</a>
                    <a href="#b">B</a>
                    <a href="#c">C</a>
                    <a href="#d">D</a>
                    <a href="#e">E</a>
                    <a href="#f">F</a>
                    <a href="#g">G</a>
                    <a href="#h">H</a>
                    <a href="#i">I</a>
                    J&nbsp;K&nbsp;
                    <a href="#l">L</a>
                    <a href="#m">M</a>
                    <a href="#n">N</a>
                    <a href="#o">O</a>
                    <a href="#p">P</a>
                    <a href="#q">Q</a>
                    <a href="#r">R</a>
                    <a href="#s">S</a>
                    <a href="#t">T</a>
                    <a href="#u">U</a>
                    V&nbsp;W&nbsp;X&nbsp;Y&nbsp;Z</span>
                    <dl class='termslist'>


                    <h3 id='a'>- A -</h3>
                    <h3>Abstract (of title)</h3>
                     <p>A written summary of the title history of a particular piece of real estate. </p>
                    <h3>Acceleration Clause</h3>
                     <p>A provision of a mortgage or note which provides that the entire outstanding balance will become due and payable in the event of default. </p>

                    <h3>Adjustable Rate Mortgage (ARM)</h3>
                     <p>A mortgage in which the interest rate is adjusted periodically, based on the movement of a financial index. </p>

                    <h3>Amortization</h3>
                     <p>Repayment of loan by installment payments. As the payments are made, the debt is reduced so that at the end of fixed period or term, no money will be owed. </p>

                    <h3>Annual Percentage Rate (APR)</h3>
                     <p>The annual percentage rate refers to the total cost of the loan, expressed as a yearly rate. </p>

                    <h3>Application Fee</h3>
                    <p>That part of the closing costs pre-paid to the lender at time of application to cover initial expenses.</p>

                    <h3>Appraisal</h3>
                     <p>A report made by a qualified person as to the value of a property as of a given date. </p>
                    <h3>Assessed Value</h3>
                     <p>The value placed on a piece of real estate by the taxing authority for the purpose of taxation. Also called an assessment. </p>

                    <h3>Assumption of Mortgage</h3>
                     <p>The purchaser takes over mortgage payments for the balance of the loan, assuming primary liability. Unless specifically released by the lender, the seller remains secondarily liable. </p>
                    <h3 id='b'>- B - </h3>
                    <!--
                    <h3>Balloon Mortgage</h3>
                     <p>A mortgage with periodic payments that do not fully amortize the loan. The outstanding balance of the mortgage is due in a lump sum at the end of the term. </p>

                    <h3>Bridge Loan</h3>
                     <p>A short-term loan secured by the equity in an as-yet-unsold house, with the funds to be used for a down payment and/or closing costs on a new house. There is no payment of principal until the house is sold or the end of the loan term, whichever comes first. Interest payments may or may not be deferred until the house is sold. </p>
                    -->
                    <h3>Broker</h3>
                     <p>The person who, for a commission or a fee, brings parties together and assists in negotiating contracts between them. </p>

                    <h3>Buydown</h3>
                     <p>Money advanced by an individual (e.g. builder, seller, buyer, lender, developer) to lower monthly mortgage payments for a few years or the whole term. </p>




                    <h3 id='c'> - C - </h3>
                    <h3>Cap (interest rate)</h3>
                     <p>The maximum interest rate increase allowable on an adjustable rate mortgage. Does not result in negative amortization. See Negative amortization. </p>

                    <h3>Cap (payment rate)</h3>
                     <p>The maximum payment amount increase allowable on an adjustable rate mortgage. May result in negative amortization. See Negative amortization. </p>

                    <h3>Certificate Of Title</h3>
                     <p>A statement that shows ownership of property, stating that the seller has clear legal title. </p>

                    <h3>Closing</h3>
                     <p>The concluding day of the real estate transaction, when title and deed pass from seller to buyer, the buyer signs the mortgage and pays the purchase price and closing costs. </p>

                    <h3>Closing Costs</h3>
                     <p>Expenses (over and above the price of the property) incurred by buyers and sellers in transferring ownership of a property. Also called settlement costs." </p>

                    <h3>Closing Disclosure (CD)</h3>
                        <p>Replaced HUD-1 Settlement Statement with the purpose of breaking the terms of your loan, monthly payments, closing costs and fees.
                    </p>

                    <h3>Closing Statement</h3>
                     <p>A financial disclosure giving an account of all funds received and expected at closing, including the escrow deposit for taxes, hazard insurance and mortgage insurance for the escrow account. </p>

                    <h3>Commission</h3>
                     <p>An agent's or broker's fee for bringing the pricipals together and helping to negotiate a real estate transaction, often a percentage of the sales price or flat fee. </p>

                    <h3>Commitment</h3>
                     <p>An agreement, frequently in writing, between a lender and a borrower to loan money at a future date, subject to certain conditions. </p>

                    <h3>Comparables</h3>
                     <p>Refers to similar properties used for comparison purposes in the appraisal process. These properties will be reasonably the same size and location, with similar amenities and characteristics, so that the approximate fair market value of the subject property can be determined. </p>

                    <h3>Condominium</h3>
                     <p>Ownership of a single unit in a multiunit building or complex of buildings. Along with this goes a share of ownership of the common areas. </p>
                    <h3>Consummation</h3>
                        <p>Previously referred to simply as "closing", the Consumer Financial Protection Bureau defines consummation as "the time that a consumer becomes contractually obligated to a credit transaction."</p>
                    <h3>Contingency</h3>
                     <p>A condition that must be met for a contract or a commitment to remain binding.</p>

                    <h3>Conventional Mortgage</h3>
                     <p>Any mortgage loan that is not insured by FHA, guaranteed by VA, of funded by a government authorized bond sale or grant. </p>

                    <h3>Convey</h3>
                     <p>To transfer real estate from one person to another. </p>

                    <h3>Credit Report</h3>
                     <p>The report to a prospective lender on the credit standing of a prospective borrower. </p>




                    <h3 id='d'>- D - </h3>

                    <h3>Deed</h3>
                     <p>A legal written document by which title to property is transferred. </p>

                    <h3>Default</h3>
                     <p>Failure to fulfill the terms as agreed to in the mortgage of note. </p>

                    <h3>Down Payment</h3>
                     <p>The difference between the sale price of a property and the mortgage amount. </p>

                    <h3>Due-On-Sale</h3>
                     <p>A clause in a mortgage which gives the lender the right to require immediate repayment of a mortgage balance if the property changes hands. </p>
                    <h3 id='e'>- E - </h3>
                    <h3>Earnest Money</h3>
                     <p>The deposit money given to seller or his agent by the potential buyer at the time of the purchase offer. If the offer is accepted, the money will become part of the down payment. </p>
                    <h3>Easement</h3>
                     <p>A right to the limited use of land owned by another. An electric company, for example, could have an easement to put up electric power lines over someone's property. </p>
                    <h3>Encumbrance</h3>
                     <p>Anything that affects or limits the title to a property, such as outstanding mortgages, easement rights or unpaid back taxes. </p>

                    <h3>Equity</h3>
                     <p>The value in which the owner has in real estate over and above the mortgages against it. When the mortgage and all other debts against the property are paid in full, the owner has 100% equity in his property. </p>
                    <h3>Escrow</h3>
                     <p>Funds and/ or deed left in trust to a third party. Generally, a portion of the monthly mortgage payment is held in escrow by the lender to pay for taxes, hazard insurance and yearly mortgage insurance premiums. </p>

                    <h3 id='f'>- F -</h3>
                    <h3>First Mortgage</h3>
                     <p>A mortgage that has a primary lien against a property. </p>

                    <h3>Fixed-Rates Mortgage</h3>
                     <p>A mortgage with an interest rate and monthly payments that remain constant over the life of the loan. </p>

                    <h3>Fixture</h3>
                     <p>Property, such as a hot water heater or plumbing fixture, that has become permanently attached to piece of real estate and goes with the property when it is sold. </p>

                    <h3>Flood Certification</h3>
                     <p>An independent agency report required by the lender to determine whether a property is located in a flood hazard zone, which would then require a federally mandated flood insurance policy. </p>

                    <h3>Foreclosure</h3>
                     <p>A legal procedure in which property mortgaged as security for a loan is sold to pay the defaulting borrower's debt. </p>

                    <h3 id='g'>- G - </h3>

                    <h3>Good Faith Estimate(GFE)</h3>
                    <p>Replaced by the Loan Estimate (LE) per RESPA-TILA law</p>

                    <h3>Graduated Payment Mortgage</h3>
                     <p>A fixed rate loan with monthly payments that start low, increasing by a fixed amount for a specific number of years. After that period, the payments typically remain constant for the duration of the loan. </p>
                    <h3>Gross Income</h3>
                     <p>Normal income, including overtime, prior to any payroll deductions, that is regular and dependable. This income may come from more than one source. </p>

                    <h3 id='h'>- H - </h3>
                    <h3>Hazard Insurance</h3>
                     <p>Insurance protection against damage to a property from fire, windstorms, and other common hazards. </p>

                    <h3>Homeowner's Insurance</h3>
                     <p>An insurance policy that covers the dwelling and its contents in case of fire or wind damage, theft, liability for property damage and personal liability. </p>

                    <!--
                    <h3>HUD-1 Form</h3>
                     <p>A form used by a settlement or closing agent itemizing all charges imposed on a borrower and seller in a real estate transaction. This form gives a picture of the closing transaction, and provides each party with a complete list of incoming and outgoing funds.</p>
                    -->

                    <h3>HUD-1 Settlement Statement (HUD)</h3>
                    <p>Replaced by the Closing Disclosure per RESPA-TILA law.</p>

                    <h3 id='i'>- I - </h3>
                    <h3>Income Property</h3>
                     <p>Real estate that is owned for investment purposes and not used as the owner's residence. </p>

                    <h3>Interest</h3>
                     <p>A charge paid for the use of money. </p>
                    <!--
                    <h3>Interim Financing</h3>
                     <p>See Bridge Loan.</p>
                    -->

                    <h3 id='l'>- L -</h3>
                    <h3>Land Contract</h3>
                     <p>When the buyer agrees to make payments directly to the seller at pre-negotiated terms. The seller agrees to deed the property to the buyer upon completion of the agreement. The buyer becomes the owner of equity in this type of sale. (Also see Owner Financing) </p>

                    <h3>Loan Estimate (LE)</h3>
                        <p>Replaced the Good Faith Estimate (GFE) and Truth-in-Lending (TIL) with the purpose of showing a summary of the loan, including interest rate, monthly payment and total closing costs..
                    </p>

                    <h3>Lien</h3>
                     <p>A legal claim on a property used as security for a debt. </p>

                    <h3>Loan-To-Value Ratio</h3>
                     <p>The relationship between the amount of the mortgage and property value, usually shown as a percentage. </p>

                    <h3 id='m'>- M - </h3>

                    <h3>Market Value</h3>
                     <p>The price at which a property will sell, assuming a knowledgeable buyer and seller, both operating without undue pressure. </p>

                    <h3>Mortgage</h3>
                     <p>A contract in which a borrower's property is pledged as security for a loan which is to be repaid on an installment basis. </p>

                    <h3>Mortgage Note</h3>
                     <p>A written promise to pay a debt at a stated interest rate during a specified term. The agreement is secured by a mortgage. </p>
                    <h3>Mortgagee</h3>
                     <p>The lender in a mortgage contract. </p>
                    <h3>Mortgagor</h3>
                     <p>The borrower in a mortgage contract.</p>

                    <h3 id='n'>- N - </h3>
                    <h3>Negative Amortization</h3>
                     <p>A loan in which the outstanding principal balance goes up instead of down because the monthly payments are not large enough to cover the full amount of interest due. Also called deferred interest. </p>

                    <h3 id='o'>- O - </h3>
                    <h3>Offer to Purchase</h3>
                     <p>A written proposal to buy a piece of real estate that becomes binding when accepted by the seller. Also called a sales contract. </p>

                    <h3>Origination Fee</h3>
                     <p>A fee charged for the work involved in the evaluation preparation and submission of a proposed mortgage loan. </p>

                    <h3>Owner Financing</h3>
                     <p>A purchase in which the seller provides all or part of the financing. </p>

                    <h3 id='p'>- P - </h3>
                    <h3>PITI</h3>
                     <p>An acronym for payments to lender that cover principal, interest, taxes and insurance on a property. </p>

                    <h3>Plat</h3>
                     <p>A map of a piece of land showing boundary lines, streets, actual measurements and easements. </p>

                    <h3>PMI (Private Mortgage Insurance)</h3>
                     <p>Insurance written by a private company to protect the lender against loss caused by mortgage default. </p>

                    <h3>Point</h3>
                     <p>A fee paid to the lender on closing day to increase the effective yield of the mortgage. A point is one percent of the amount of the mortgage loan. Also called a discount point. </p>

                    <h3>Pre-Approval</h3>
                     <p>A commitment by a lender to extend credit provided that specific conditions are met. </p>

                    <h3>Prepayment Penalty</h3>
                     <p>A charge paid to the lender by the borrower if a mortgage loan is repaid before its term is over. </p>

                    <h3>Pre-Qualification</h3>
                     <p>A preliminary assessment of a buyer's ability to secure a loan, based on a specific set of lending guidelines and buyer representations made. This is not a guarantee or commitment by a lender to extend credit.</p>

                    <h3>Prime Rate</h3>
                     <p>The interest rate charged by banks to their preferred corporate customers, it tends to be an estimator for general trends in short term interest rates. </p>
                    <h3>Principal</h3>
                     <p>The amount borrowed or remaining unpaid; also, that part of the monthly payment that reduces the outstanding balance of a mortgage. </p>



                    <h3 id='q'>- Q - </h3>

                    <h3>Qualifying Ratios</h3>
                     <p>Guidelines applied by lenders to determine how large a loan to grant a home buyer. </p>
                    <h3 id='r'>- R - </h3>

                    <h3>Realtor</h3>
                     <p>A real estate broker or sales associate affiliated with the National Association of Realtors. </p>

                    <h3>Registration Fee</h3>
                     <p>The charges made by the register of deeds to record the legal documents. </p>

                    <h3>Refinancing</h3>
                     <p>Repaying a debt with the proceeds of a new loan, using the same property as collateral or security. </p>

                    <h3 id='s'>- S - </h3>

                    <h3>Second Mortgage</h3>
                     <p>A loan on property which already has an existing mortgage(the first mortgage). The second mortgage is subordinate to the first. </p>

                    <h3>Secondary Mortgage Market</h3>
                     <p>The buying and selling of existing mortgages through agencies (i.e. Fannie Mae, Freddie Mac). </p>
                    <h3>Survey</h3>
                     <p>A map prepared by an engineer or surveyor charting a particular piece of real estate. </p>

                    <h3 id='t'>- T - </h3>
                    <h3>Title</h3>
                     <p>Ownership of a property. A clear title is one without any outstanding liens or encumbrances. A cloud on title refers to any outstanding liens or encumbrances which could impair the title. </p>
                    <h3>Title Insurance Policy</h3>
                     <p>Protection against financial loss arising from defects in the title occurring before purchase. </p>

                    <h3>Title Search</h3>
                     <p>A check of public record to disclose the past and current facts regarding ownership of a particular piece of property. </p>

                    <h3>Transfer Tax</h3>
                     <p>In some areas city, county or state taxes imposed when property passes from one person to another. </p>

                    <h3>Truth-In-Lending</h3>
                        <p>Replaced by the Loan Estimate (LE) per RESPA-TILA law.</p>
                    <!-- <p>A federal law that requires lenders to fully disclose, in writing, the terms and conditions of a mortgage, including the APR and other charges. </p>-->

                    <h3 id='u'>- U - </h3>

                    <h3>Underwriting</h3>
                     <p>The process of evaluating a loan application to determine the risk involved for the lender.</p>
                    <div style="margin-top: 60px">
                       <a href="#glossary-nav">Back to top</a>
                    </div>

                </div>
            </div>
          </div>
      </div>


@stop
