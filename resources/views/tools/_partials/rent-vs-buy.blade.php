<section id="rent-vs-buy">
<div class="container">
<div class="row body-margin-top">

<h1 class="text-center">
  Renting vs. Buying Calculator
</h1>

<br>

<h4>
  Should you buy or rent? This is a question most of us will likely face in our lives, whether buying a house makes more financial sense than renting a home. There is a way to understand the financial impact of buying vs. renting. This rent vs. buy calculator can help you calculate the net cost of buying a home versus the cost of renting over time.
</h4>

<br>

<form>

  <p>

    <label for="homePrice">Desired Home Price</label>
    <br />

    <input type="text" id="homePrice" value="100000" style="font-size:18px;padding:5px;" />

  </p>

  <p>

    <label for="monthlyRent">Desired Monthly Rent</label>
    <br />

    <input type="text" id="monthlyRent" min='0' max='12' value="800" style="font-size:18px;padding:5px;" />

  </p>

  <p>
    <input type="button" class="btn btn-lg main-btn-form" value="Calculate" onclick="rentCalc()" />
  </p>

</form>

<p id="amount"></p>

<p style="margin-top:40px;font-size:10px;border-top:1px solid #444;">

  <em>
    This loan calculator assumes compounding and payments occur monthly.  Your actual loan may vary but this estimate should still give you a good idea of about how much you can afford. The results of this loan payment calculator are for comparison purposes only.
  </em>

</p>


</div>
</div>
</section>
