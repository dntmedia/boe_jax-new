<section id="affordability">
<div class="container">
<div class="row body-margin-top">


<h2 class="text-center">
	Loan Affordability Calculator
</h2>

<br>

<h4>
How much house you can afford? Estimate the mortgage amount that best fits your budget with our new house calculator
</h4>

<br>

<form>

	<p>


		<label for="loanpayments">Desired Monthly Payment</label>
		<br />
		
		<input type="text" id="loanpayments" value="1000" style="font-size:18px;padding:5px;" />

	</p>

	<p>

		<label for="loanrate">Applicable Interest Rate (% per Year)</label>
		<br />

		<input type="number" id="loanrate" min='0' max='12' value="4.25" style="font-size:18px;width:90px;padding:5px 10px;" /> %

	</p>

	<p>

		<label for="loanterm">Desired Loan Term</label>
		<br />

		<select id="loanterm" style="font-size:18px;padding:5px;">
			<option value="5">5 years</option>
			<option value="10">10 years</option>
			<option value="15">15 years</option>
			<option value="20">20 years</option>
			<option value="25">25 years</option>
			<option selected value="30">30 years</option>
		</select>

	</p>

	<p>

		<input type="button" class="btn btn-lg main-btn-form" value="Calculate" onclick="affordCalc()" />

	</p>

</form>

<p id="amount"></p>

<p style="margin-top:40px;font-size:10px;border-top:1px solid #444;">

	<em>
	This loan calculator assumes compounding and payments occur monthly.  Your actual loan may vary but this estimate should still give you a good idea of about how much you can afford. The results of this loan payment calculator are for comparison purposes only.
	</em>

</p>


</div>
</div>
</section>
