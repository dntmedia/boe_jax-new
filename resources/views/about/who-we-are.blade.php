@extends('_layouts.app')

@section('keywords')
keywords
@stop
@section('description')
description
@stop


{{-- Web site Title --}}
@section('title')
Who We Are @parent
@stop

@section('styles')
@stop

@section('scripts')
@stop

@section('inline-scripts')
@stop


{{-- Content --}}
@section('content')

<section id="about" style="margin-top: 95px">
  <img src="{{ url('images/home/jax_header.jpg') }}" alt="Bank of England Mortgage Jacksonville, Florida downtown" class="img-responsive">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="who-we-are-title">
          <h1>About BOE Mortgage</h1>
        </div>
        <div class="who-we-are-body">
          <p>At Bank of England Mortgage, we take pride in being there when our borrowers need us, day and night. Our mission is to deliver total value to our customers. This means competitive rates, STRESS-FREE closings, extensive product knowledge, and a friendly helpful attitude.</p>
          <p>Bank of England Mortgage offers the strengths of a strong capital base, a solid and committed business plan and experienced management along with the assurance of a successful track record.</p>
          <p>Since our doors opened in 1898 in England, Arkansas, Bank of England Mortgage has been providing down home service. Along with powerful nationwide mortgage loans, we offer clients peace of mind knowing they are working with a credible national mortgage banker. Our success is due primarily to the talent of our people and access to hundreds of mortgage products at industry best pricing. Our management team has a combined 150 years of mortgage experience and is actively involved as members and board members on local, state and national mortgage broker and banker associations.</p>
          <p>Building on that foundation, we have grown to a company of more than 1000 employees with branches in over 39 states. We have 93 locations nationwide to serve your mortgage needs.</p>
          <p></p>
          <div id="about_jax"></div>
          <hr>
        </div>
      </div>
    </div>
  </div>
</section>

@stop
