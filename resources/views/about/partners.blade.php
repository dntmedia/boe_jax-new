@extends('_layouts.app')

@section('keywords')
keywords
@stop
@section('description')
description
@stop


{{-- Web site Title --}}
@section('title')
Our Partners @parent
@stop

@section('styles')
@stop

@section('scripts')
@stop

@section('inline-scripts')
@stop


{{-- Content --}}
@section('content')

<div style="margin-top: 95px;"></div>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="about-title">
          <h1 class="text-center">BOE Mortgage Partners</h1>
          <p class="lead text-center" style="margin-bottom: 40px">BOE Mortgage supports and promotes local business and entrepreneurs.</p>
        </div>
        <div class="about-body">
          <div class="containers">
            <div class="row">
              <div class="col-lg-4 col-md-4 col-sm-6">
                <div class="thumbnail">
                  <a href="http://www.bninefl.com/" target="_blank"><img src="{{ url('images/about/partners/BNI.jpg') }}" alt="BNI"></a>
                  <div class="caption partner-title">
                    <p>BNI</p>
                  </div>
                </div>
              </div>
              <div class="col-lg-4 col-md-4 col-sm-6">
                <div class="thumbnail">
                  <a href="http://www.davidweekleyhomes.com/" target="_blank"><img src="{{ url('images/about/partners/DavidWeeklyHomes.jpg') }}" alt="David Weekley Homes"></a>
                  <div class="caption partner-title">
                    <p>David Weekley Homes</p>
                  </div>
                </div>
              </div>
              <div class="col-lg-4 col-md-4 col-sm-6">
                <div class="thumbnail">
                  <a href="http://www.davidsonrealtyinc.com/" target="_blank"><img src="{{ url('images/about/partners/DavidsonRealty.jpg') }}" alt="Davidson Realty"></a>
                  <div class="caption partner-title">
                    <p>Davidson Realty</p>
                  </div>
                </div>
              </div>
              <div class="col-lg-4 col-md-4 col-sm-6">
                <div class="thumbnail">
                  <a href="http://www.exitrealestategallery.com/" target="_blank"><img src="{{ url('images/about/partners/ExitRealEstate.jpg') }}" alt="Exit Real Estate Gallery"></a>
                  <div class="caption partner-title">
                    <p>Exit Real Estate Gallery</p>
                  </div>
                </div>
              </div>
              <div class="col-lg-4 col-md-4 col-sm-6">
                <div class="thumbnail">
                  <a href="http://icihomes.com/a/1/jacksonville/" target="_blank"><img src="{{ url('images/about/partners/ICIHomes.jpg') }}" alt="ICI Homes"></a>
                  <div class="caption partner-title">
                    <p>ICI Homes</p>
                  </div>
                </div>
              </div>
              <div class="col-lg-4 col-md-4 col-sm-6">
                <div class="thumbnail">
                  <a href="http://www.myjaxchamber.com/index.php?src=gendocs&ref=ImpactJAX&category=_get_involved" target="_blank"><img src="{{ url('images/about/partners/JAXChamber.jpg') }}" alt="Impact JAX"></a>
                  <div class="caption partner-title">
                    <p>Impact JAX</p>
                  </div>
                </div>
              </div>
              <div class="col-lg-4 col-md-4 col-sm-6">
                <div class="thumbnail">
                  <a href="http://www.kw.com/kw/" target="_blank"><img src="{{ url('images/about/partners/KellerWilliams.jpg') }}" alt="Keller Williams Atlantic Partners (Amelia Island)"></a>
                  <div class="caption partner-title">
                    <p>Keller Williams Atlantic Partners (Amelia Island)</p>
                  </div>
                </div>
              </div>
              <div class="col-lg-4 col-md-4 col-sm-6">
                <div class="thumbnail">
                  <a href="http://daytonabeach.yourkwoffice.com/mcj/user/home.html" target="_blank"><img src="{{ url('images/about/partners/KellerWilliams.jpg') }}" alt="Keller Williams Atlantic Partners (Daytona)"></a>
                  <div class="caption partner-title">
                    <p>Keller Williams Atlantic Partners (Daytona)</p>
                  </div>
                </div>
              </div>
              <div class="col-lg-4 col-md-4 col-sm-6">
                <div class="thumbnail">
                  <a href="http://www.jacksonvillekw.com/" target="_blank"><img src="{{ url('images/about/partners/KellerWilliams.jpg') }}" alt="Keller Williams Atlantic Partners (Jacksonville Beach)"></a>
                  <div class="caption partner-title">
                    <p>Keller Williams Atlantic Partners (Jax Beach)</p>
                  </div>
                </div>
              </div>
              <div class="col-lg-4 col-md-4 col-sm-6">
                <div class="thumbnail">
                  <a href="http://kwjaxsouthside.yourkwoffice.com/" target="_blank"><img src="{{ url('images/about/partners/KellerWilliams.jpg') }}" alt="Keller Williams Atlantic Partners (Southside)"></a>
                  <div class="caption partner-title">
                    <p>Keller Williams Atlantic Partners (Southside)</p>
                  </div>
                </div>
              </div>
              <div class="col-lg-4 col-md-4 col-sm-6">
                <div class="thumbnail">
                  <a href="http://www.nefar.com/" target="_blank"><img src="{{ url('images/about/partners/NEFAR.jpg') }}" alt="NEFAR"></a>
                  <div class="caption partner-title">
                    <p>NEFAR</p>
                  </div>
                </div>
              </div>
              <div class="col-lg-4 col-md-4 col-sm-6">
                <div class="thumbnail">
                  <a href="http://www.nefba.com/" target="_blank"><img src="{{ url('images/about/partners/NEFBA.jpg') }}" alt="NEFBA"></a>
                  <div class="caption partner-title">
                    <p>NEFBA</p>
                  </div>
                </div>
              </div>
              <div class="col-lg-4 col-md-4 col-sm-6">
                <div class="thumbnail">
                  <a href="http://www.wcr.org/" target="_blank"><img src="{{ url('images/about/partners/WCR.jpg') }}" alt="Women's Council of Realtors"></a>
                  <div class="caption partner-title">
                    <p>Women's Council of Realtors</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


@stop
