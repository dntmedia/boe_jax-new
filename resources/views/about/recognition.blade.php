@extends('_layouts.app')

@section('keywords')
keywords
@stop
@section('description')
description
@stop


{{-- Web site Title --}}
@section('title')
Recognitions @parent
@stop

@section('styles')
@stop

@section('scripts')
@stop

@section('inline-scripts')
@stop


{{-- Content --}}
@section('content')
<div style="margin-top: 95px;">
  <img src="{{ url('images/about/recognitions/header.jpg') }}" alt="Recognition Awards BOE Mortgage Jax" class="img-responsive">
</div>
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="about-title">
        <h1 class="text-center">BOE Jacksonville Recognition</h1>
      </div>
      <div class="about-body">
        <div class="containers">
          <div class="row">
            <h3 class="text-center">2016 Jacksonville Business Journal's Best Places to Work</h3>
              <div class="feature-content">
                <div class="col-lg-6 text-center">
                  <img src="{{ url('images/about/recognitions/bptw-16.jpg') }}" alt="Best Places to work logo">
                </div>
                <div class="col-lg-6">
                  <img src="{{ url('images/about/recognitions/BPTW-2016.jpg') }}" alt="BOE Mortgage Jacksonville Team BPTW 2016" class="img-responsive">
                </div>
              </div>
              <h3 class="text-center">2015 Jacksonville Business Journal's Best Places to Work</h3>
              <div class="feature-content">
                <div class="col-lg-6 text-center col-lg-push-6">
                  <img src="{{ url('images/about/recognitions/15-bptw-badge.png') }}" alt=" 2015 Best Places to work logo">
                </div>
                <div class="col-lg-6 col-lg-pull-6">
                  <img src="{{ url('images/about/recognitions/Best-Places-to-Work-Award-600.jpg') }}" alt="BOE Mortgage Jacksonville Team BPTW 2015" class="img-responsive">
                </div>
              </div>
                <em>“For more than a decade, the <a href="http://www.bizjournals.com/jacksonville/print-edition/2015/06/26/bank-of-england.html" target="_blank">Jacksonville Business Journal</a> has helped identify the companies that have the more interesting workplaces, the best culture, the happiest employees. Working with our partner, national research firm Quantum Workplace, we've poked and prodded, surveyed and measured and — in the end — come down to the companies that truly stand out.”</em>
              <div style="padding-top: 10px; border-bottom: 2px solid black"></div>
          </div>
          <div class="row">
            <h3 class="text-center">Laurel Awards</h3>
              <div class="text-center">
                <img src="{{ url('images/about/recognitions/Laurel_Lender_2015.png') }}" alt="2015 Laurel Award">
                <img src="{{ url('images/about/recognitions/Laurel_website_2015.png') }}" alt="BOE Mortgage Jacksonville Laurel Web Page of the Year 2015">
                <img src="{{ url('images/about/recognitions/Laurel_Logo_2015.png') }}" alt="BOE Mortgage Jacksonville Laurel Logo of the Year 2015">
                <img src="{{ url('images/about/recognitions/Laurel_facebook_2015.png') }}" alt="BOE Mortgage Jacksonville Team Facebook Page of the Year 2015">
                <img src="{{ url('images/about/recognitions/Laurel_radio_2015.png') }}" alt="BOE Mortgage Jacksonville Team Laurel Award Radio Ad of the Year 2015">
              </div>
              <div class="flex-content" style="margin-top: 30px">
                <div class="col-md-6">
                  <img src="{{ url('images/about/recognitions/2015-LaurelAwards.jpg') }}" alt="2015 Laurel Award Team Pic" class="img-responsive">
                </div>
                <div class="col-md-6">
                  <h4><strong>2015 Annual Sales and Marketing Council Laurel Awards:</strong></h4>
                  <ul style="list-style: none; padding: 0; margin: 0">
                    <li>
                      <i class="fa fa-trophy" aria-hidden="true"></i>
                        Top Producer for Mortgage Loan Officer - Jason Kindler
                    </li>
                    <li>
                      <i class="fa fa-trophy" aria-hidden="true"></i>
                        Million Dollar Circle, $42million - Jason Kindler
                    </li>
                    <li>
                      <i class="fa fa-trophy" aria-hidden="true"></i>
                        Lender of the Year - Jason Kindler
                    </li>
                    <li>
                      <i class="fa fa-trophy" aria-hidden="true"></i>
                        Excellence in Marketing, Best Logo
                    </li>
                    <li>
                      <i class="fa fa-trophy" aria-hidden="true"></i>
                        Excellence in Marketing, Best Website
                    </li>
                    <li>
                      <i class="fa fa-trophy" aria-hidden="true"></i>
                        Excellence in Marketing, Best Facebook
                    </li>
                    <li>
                      <i class="fa fa-trophy" aria-hidden="true"></i>
                        Excellence in Marketing, Best Radio Ad
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <p>The Sales & Marketing Council (SMC), a council of the Northeast Florida Builder’s Association (NEFBA) awards builders, developers, architects, planners, sales and marketing professionals, interior designers, Realtors and lenders on excellence throughout the year.</p>
          </div>
          <div class="row">
            <div class="col-md-4">
              <img src="{{ url('images/about/recognitions/Laurel-Awards-Lender-of-the-Year-2015.jpg') }}" alt="2015 Laurel Award Team Pic" class="img-responsive">
            </div>
            <div class="col-md-4">
              <img src="{{ url('images/about/recognitions/Laurel-Award-Logo-of-the-Year-2015.jpg') }}" alt="2015 Laurel Award Team Pic" class="img-responsive">
            </div>
            <div class="col-md-4">
              <img src="{{ url('images/about/recognitions/Laurel-Awards-Website-of-the-Year-2015-Chelsea.jpg') }}" alt="2015 Laurel Award Team Pic" class="img-responsive">
            </div>
          </div>
          <div style="padding-top: 30px; border-bottom: 2px solid black"></div>
          <div class="row">
            <h3 class="text-center">Top Mortgage Lenders 2014</h3>
            <p class="text-center">Ranked 6th among Mortgage Lenders in Jacksonville by Volume of Mortgages by Area Offices</p>
          </div>
        </div>
      </div>
    </div>
  </div>


@stop
