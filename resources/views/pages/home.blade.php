@extends('_layouts.app')

@section('keywords')
keywords
@stop
@section('description')
description
@stop


{{-- Web site Title --}}
@section('title')
Home @parent
@stop

@section('styles')
	<link rel="stylesheet" type="text/css" href="{{ url('assets/css/lightslider.css') }}">
@stop

@section('scripts')
	<script type="text/javascript" src="{{ url('assets/vendors/lightslider/js/lightslider.min.js') }}"></script>
	<script type="text/javascript" src="{{ url('assets/js/light_slider.js') }}"></script>
@stop

@section('inline-scripts')
@stop


{{-- Content --}}
@section('content')
<section id="lead">
  <div class="row">
    <div class="col-lg-12">
      <div id="lead-image">
        <img src="{{ url('images/home/jax_header.jpg') }}" alt="Bank of England Mortgage Jacksonville" class="img-responsive">
      </div>
      <div id="lead-content" class="animated fadeInUp">
        <h1>Welcome to Bank of England Mortgage Jacksonville</h1>
        <p>Closings you can bank on</p>
      </div>
    </div>
  </div>
</section>
<section id="tools">
  <div class="container">
    <div class="row">
      <div class="col-md-3 col-sm-6">
        <div class="thumbnail home-thumbnail">
          <a href="/community" target="_blank"><img src="{{ url('images/home/community_thumbnail.jpg') }}" alt="BOE Jax community" class="img-responsive"></a>
          <div class="caption ">
            <h3 class="text-center">Community</h3>
            <p></p>
            <p class="text-center"><a class="btn main-btn" href="/community">Learn More <i class="fa fa-angle-double-right" aria-hidden="true"></i></a></p>
          </div><!-- /.caption -->
        </div><!-- /.thumbnail -->
      </div>
      <div class="col-md-3 col-sm-6">
        <div class="thumbnail home-thumbnail">
          <a href="/about/partners"><img src="{{ url('images/home/partners_thumbnail.jpg') }}" alt="BOE Mortgage Florida Partners"></a>
          <div class="caption">
            <h3 class="text-center">Our Partners</h3>
            <p></p>
            <p class="text-center"><a class="btn main-btn" href="/about/partners">Learn More <i class="fa fa-angle-double-right" aria-hidden="true"></i></a></p>
          </div><!-- /.caption -->
        </div><!-- /.thumbnail -->
      </div>
      <div class="col-md-3 col-sm-6">
        <div class="thumbnail home-thumbnail">
          <a href="/videos"><img src="{{ url('images/home/videos_thumbnail.jpg') }}" alt="BOE Jax Videos"></a>
          <div class="caption">
            <h3 class="text-center">Videos</h3>
            <p></p>
            <p class="text-center"><a class="btn main-btn" href="/videos">View More <i class="fa fa-angle-double-right" aria-hidden="true"></i></a></p>
          </div><!-- /.caption -->
        </div><!-- /.thumbnail -->
      </div>
      <div class="col-md-3 col-sm-6">
        <div class="thumbnail home-thumbnail">
          <a href="/tools/fastapp"><img src="{{ url('images/home/fastapp_thumbnail.jpg') }}" alt="BOE FastApp available for download"></a>
          <div class="caption">
            <h3 class="text-center">BOE FastApp</h3>
            <p></p>
            <p class="text-center"><a class="btn main-btn" href="/tools/fastapp">Learn More <i class="fa fa-angle-double-right" aria-hidden="true"></i></a></p>
          </div><!-- /.caption -->
        </div><!-- /.thumbnail -->
      </div>
      <div id="branches"></div>
    </div>
  </div>
</section>
<section id="team">
  <div class="row" >
    <div class="col-lg-12">
      <div id="team-photo">
        <img src="{{ url('images/home/jax_team2.jpg') }}" alt="Bank of England Mortgage Jacksonville Team" class="img-responsive">
      </div>
      <div class="container branches-section hidden-xs show-sm">
        <h2 class="text-center">Bank of England Mortgage Jacksonville Branches</h2>
          <div class="row text-center">
            <div class="col-lg-6 col-md-6 col-sm-6">
              <div class="fadeIn animated fadeIn">
                <h3>
                  <a href="/about/branch/1114">
                    <address>
                      <i class="fa fa-map-marker" aria-hidden="true"></i>
                        13901 Sutton Park Drive S. Suite 170
                    </address>
                  </a>
                </h3>
              </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6">
              <div class="fadeIn animated fadeIn">
                <h3>
                  <a href="/about/branch/1112">
                    <address>
                      <i class="fa fa-map-marker" aria-hidden="true"></i>
                        13901 Sutton Park Drive S. Suite 403
                    </address>
                  </a>
                </h3>
              </div>
            </div>
          </div><!-- end row -->
        </div><!-- end container -->
        <div id="team-content" class="animated rollIn show-xs hidden-sm hidden-md hidden-lg">
          <div>
            <h3>
              <a href="/about/branches">Meet the branches <i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i></a>
            </h3>
          </div>
        </div>
      </div>
    </div>
</section>
<section id="products">
  <div class="container">
    <h2 class="">Loan Programs</h2>
      <ul id="lightSlider">
        <li>
          <div class="thumbnail product-thumb">
            <a href="/products/conventional"><img src="{{ url('images/home/conventional_loan_icon_blue.png') }}" class="icon-spin" alt="Bank of England Mortgage Conventional Loans"></a>
            <div class="caption">
              <h3 class="text-center">Conventional Loans</h3>
              <p class="text-center"><a class="btn main-btn" href="/products/conventional">Learn More <i class="fa fa-angle-double-right" aria-hidden="true"></i></a></p>
            </div><!-- /.caption -->
          </div><!-- /.thumbnail -->
        </li> 
        <li>
          <div class="thumbnail product-thumb">
              <a href="/products/fha-loans"><img src="{{ url('images/home/fha_loan_icon_blue.png') }}" class="icon-spin" alt="Bank of England Mortgage FHA Loans"></a>
              <div class="caption">
                <h3 class="text-center">FHA Loans</h3>
                <p class="text-center"><a class="btn main-btn" href="/products/fha-loans">Learn More <i class="fa fa-angle-double-right" aria-hidden="true"></i></a></p>
              </div><!-- /.caption -->
          </div><!-- /.thumbnail -->
        </li>
        <li>
          <div class="thumbnail product-thumb">
            <a href="/products/va-loans"><img src="{{ url('images/home/va_loan_icon_blue.png') }}" class="icon-spin" alt="Bank of England Mortgage VA Loans"></a>
            <div class="caption">
              <h3 class="text-center">VA Loans</h3>
              <p class="text-center"><a class="btn main-btn" href="/products/va-loans">Learn More <i class="fa fa-angle-double-right" aria-hidden="true"></i></a></p>
            </div><!-- /.caption -->
          </div><!-- /.thumbnail -->
        </li>  
        <li>
          <div class="thumbnail product-thumb">
            <a href="/products/renovation"><img src="{{ url('images/home/renovation_loan_icon_blue.png') }}" class="icon-spin" alt="Bank of England Mortgage Renovation"></a>
            <div class="caption">
              <h3 class="text-center">Renovation</h3>
              <p class="text-center"><a class="btn main-btn" href="/products/renovation">Learn More <i class="fa fa-angle-double-right" aria-hidden="true"></i></a></p>
            </div><!-- /.caption -->
          </div><!-- /.thumbnail -->
        </li>
        <li>
          <div class="thumbnail product-thumb">
            <a href="/products/usda-loans"><img src="{{ url('images/home/usda_loan_icon_blue.png') }}" class="icon-spin" alt="Bank of England Mortgage USDA Loans"></a>
            <div class="caption">
              <h3 class="text-center">USDA Loans</h3>
              <p class="text-center"><a class="btn main-btn" href="/products/usda-loans">Learn More <i class="fa fa-angle-double-right" aria-hidden="true"></i></a></p>
            </div><!-- /.caption -->
          </div><!-- /.thumbnail -->
        </li>
        <li>
          <div class="thumbnail product-thumb">
            <a href="/products/reverse-mortgage"><img src="{{ url('images/home/reverse_mortgage_icon_blue.png') }}" class="icon-spin" alt="Bank of England Mortgage Reverse Mortgage"></a>
            <div class="caption">
              <h3 class="text-center">Reverse Mortgage</h3>
              <p class="text-center"><a class="btn main-btn" href="/products/reverse-mortgage">Learn More <i class="fa fa-angle-double-right" aria-hidden="true"></i></a></p>
            </div><!-- /.caption -->
          </div><!-- /.thumbnail -->
        </li>
        <li>
          <div class="thumbnail product-thumb">
            <a href="/products/jumbo-loans"><img src="{{ url('images/home/jumbo_loan_icon_blue.png') }}" class="icon-spin" alt="Bank of England Mortgage Jumbo Loans"></a>
            <div class="caption">
              <h3 class="text-center">Jumbo Loans</h3>
              <p class="text-center"><a class="btn main-btn" href="/products/jumbo-loans">Learn More <i class="fa fa-angle-double-right" aria-hidden="true"></i></a></p>
            </div><!-- /.caption -->
          </div><!-- /.thumbnail -->
        </li>
        <li>
          <div class="thumbnail product-thumb">
            <a href="/products/vacation"><img src="{{ url('images/home/vacation_loan_icon_blue.png') }}" class="icon-spin" alt="Bank of England Mortgage Vacation/Second Homes"></a>
            <div class="caption">
              <h3 class="text-center">Vacation/Second Home</h3>
              <p class="text-center"><a class="btn main-btn" href="/products/vacation">Learn More <i class="fa fa-angle-double-right" aria-hidden="true"></i></a></p>
            </div><!-- /.caption -->
          </div><!-- /.thumbnail -->
        </li>
        <li>
          <div class="thumbnail product-thumb">
            <a href="/products/bond-loans"><img src="{{ url('images/home/bond_loan_icon_blue.png') }}" class="icon-spin" alt="Bank of England Mortgage Bond Loans"></a>
            <div class="caption">
              <h3 class="text-center">Bond Loans</h3>
              <p class="text-center"><a class="btn main-btn" href="/products/bond-loans">Learn More <i class="fa fa-angle-double-right" aria-hidden="true"></i></a></p>
            </div><!-- /.caption -->
          </div><!-- /.thumbnail -->
        </li>
        <li>
          <div class="thumbnail product-thumb">
            <a href="/products/bridge-loans"><img src="{{ url('images/home/bridge_loan_icon_blue.png') }}" class="icon-spin" alt="Bank of England Mortgage Bridge Loans"></a>
            <div class="caption">
              <h3 class="text-center">Bridge Loans</h3>
              <p class="text-center"><a class="btn main-btn" href="/products/bridge-loans">Learn More <i class="fa fa-angle-double-right" aria-hidden="true"></i></a></p>
            </div><!-- /.caption -->
          </div><!-- /.thumbnail -->
        </li>
      </ul>
  </div>
</section>
<section style="padding:4rem 0;">
  <div class="row">
    <div class="container">
      <p class="social-title">Like, Share, and Explore with Us. We're passionate about our community.  <i class="fa fa-facebook-square" aria-hidden="true"></i>  <i class="fa fa-twitter-square" aria-hidden="true"></i></p>
      <div class="col-md-8 col-md-offset-2">
        <div class="col-lg-6 col-md-6">
          <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fthebankofengland%2F&tabs=timeline&width=340&height=575&adapt_container_width=true&hide_cover=true&show_facepile=false&appId" width="340" height="575" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
        </div>
        <div class="col-lg-6 col-md-6">
          <div class="hp-twitter">
            <a class="twitter-timeline" data-width="340" data-height="575" href="https://twitter.com/boejax">Tweets by boejax</a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section id="hp-testimonials">
  <div class="row">
    <div class="container">
      <ul id="lightSlider2">
        @foreach($featured as $testimonial)
        <li>
          <div class="thumbnail testimonial-thumbnail">
            <blockquote>
              <em>{{ $testimonial->summary }}</em>
              <footer>
                <cite title="{{ $testimonial->person }}">{{ $testimonial->person }}</cite>
              </footer>
            </blockquote>
          </div><!-- /.thumbnail -->
        </li>
        @endforeach
      </ul>
    </div>
  </div>
</section>

@stop
