@extends('_layouts.app')

@section('keywords')
keywords
@stop
@section('description')
description
@stop


{{-- Web site Title --}}
@section('title')
Loan Process @parent
@stop

@section('styles')
@stop

@section('scripts')
@stop

@section('inline-scripts')
@stop


{{-- Content --}}
@section('content')
<div style="margin-top: 95px;"></div>
  <div class="container">
    <h1 class="text-center">Your New Loan Process</h1>
    <div class="row">
      <div class="col-md-8 col-md-offset-2">
        <ul class="loan-process-list">
          <li>Your Loan Process</li>
          <li>Your Bank of England Mortgage Team is dedicated to making your home purchase or refinance a smooth process. </li>
        </ul>
      </div>
    </div>
    <div class="row" style="margin: 40px 0 60px">
      <div class="col-md-4">
        <img src="{{ url('images/~images/loan_process1.png') }}" alt="" class="img-responsive">
      </div>
      <div class="col-md-4">
        <img src="{{ url('images/~images/loan_process2.png') }}" alt="" class="img-responsive">
      </div>
      <div class="col-md-4">
        <img src="{{ url('images/~images/loan_process3.png') }}" alt="" class="img-responsive">
      </div>
    </div>
    <hr>
    <div class="row" style="margin: 40px 0">
      <div class="col-md-8 col-md-offset-2">
        <div>
          <img src="{{ url('images/~images/LETime.png') }}" alt="" class="img-responsive">
        </div>
        <div>
          <img src="{{ url('images/~images/CDTime.png') }}" alt="" class="img-responsive">
        </div>
      </div>
    </div>
    <hr>
    <div class="row" style="margin-bottom: 50px">
      <h3>More Information</h3>
        <div style="text-align: center">
          <h5>Consumer Financial Protection Bureau Brochure</h5>
            <a href="{{ url('assets/PDFs/CFPB-Booklet.pdf') }}">
              <img src="{{ url('images/~images/CFPB.jpg') }}" alt="Consumer Financial Protection Bureau Brochure" class="img-responsive text-center">
            </a>
        </div>
    </div>
  </div>




@stop
