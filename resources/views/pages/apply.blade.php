@extends('_layouts.app')

@section('keywords')
keywords
@stop
@section('description')
description
@stop


{{-- Web site Title --}}
@section('title')
Apply @parent
@stop

@section('styles')
@stop

@section('scripts')
@stop

@section('inline-scripts')
@stop


{{-- Content --}}
@section('content')

<div style="margin-top: 95px;"></div>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="about-title">
          <h1>Apply Now!</h1>
          <p class="lead text-center" style="margin-bottom: 40px">Bank of England Mortgage is dedicated to finding you the mortgage you need. Whether it is a new home mortgage, refinancing an existing mortgage or a home equity loan we know where to look to find the best deal for you. Fill out the information below to get started today.</p>
        </div>
        <div class="about-body">
          <div class="container" style="background-color: #f6f6f6; padding: 3rem">
            <p><script type="text/javascript">var host = (("https:" == document.location.protocol) ? "https://secure." : "http://");document.write(unescape("%3Cscript src='" + host + "wufoo.com/scripts/embed/form.js' type='text/javascript'%3E%3C/script%3E"));</script>

              <script type="text/javascript">
                var z7x3p3 = new WufooForm();
                z7x3p3.initialize({
                'userName':'boejax',
                'formHash':'z7x3p3',
                'autoResize':true,
                'height':'2043',
                'ssl':true});
                z7x3p3.display();
                </script>
            </p>
          </div>
        </div>
      </div>
    </div>
  </div>

@stop
