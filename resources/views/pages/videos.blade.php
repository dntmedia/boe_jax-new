@extends('_layouts.app')

@section('keywords')
keywords
@stop
@section('description')
description
@stop


{{-- Web site Title --}}
@section('title')
Videos @parent
@stop

@section('styles')
{{--
  <link rel="stylesheet" type="text/css" href="{{ url('assets/vendors/video-js-5.14.1/video-js.min.css') }}">
 --}}
@stop

@section('scripts')
  <script src="//vjs.zencdn.net/ie8/1.1.1/videojs-ie8.min.js"></script>



  <script type="text/javascript" src="{{ url('assets/vendors/video-js/libs/swfobject.js') }}"></script>
  <script type="text/javascript" src="{{ url('assets/vendors/video-js/libs/jwplayer.js') }}"></script>
@stop

@section('inline-scripts')
window.HELP_IMPROVE_VIDEOJS = false;
        jwplayer('mediaspace').setup({
        'flashplayer': '/assets/vendors/video-js/libs/player.swf',
                    'playlistfile': '/assets/videos/master_mrss.xml',
                    'playlist.position': 'bottom',
                    'backcolor': '0x000000',
                    'frontcolor': '0xCCCCCC',
                    'lightcolor': '0xd9bb40',
                    'autostart': 'false',
                    'stretching': 'exactfit',
                    'controlbar': 'bottom',
                    'width': 'auto',
                    'height': '850'
        });
@stop


{{-- Content --}}
@section('content')



<section id="about" style="margin-top: 95px">
    <!-- <img src="{{ url('images/~images/take-roll.jpg') }}" alt="Bank of England Mortgage Jacksonville Videos" class="img-responsive"> -->
    <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="who-we-are-title">
          <h1> <i class="fa fa-film" aria-hidden="true"></i> Jacksonville Videos</h1>
        </div>


        <div id="mediaspace">This text will be replaced</div>


          <hr>
      </div>
  </div>
</section>

@stop
