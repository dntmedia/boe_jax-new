@extends('_layouts.app')

@section('keywords')
keywords
@stop
@section('description')
description
@stop


{{-- Web site Title --}}
@section('title')
Our Community @parent
@stop

@section('styles')
@stop

@section('scripts')
@stop

@section('inline-scripts')
@stop


{{-- Content --}}
@section('content')


        <div style="margin-top: 95px;">
          <div class="container">
            <h1 class="text-center">Jacksonville Community</h1>
            <div class="row">
              <div class="flex-content">
                <div class="col-md-6">
                  <img src="{{ url('images/community/rttfundraiser.jpg') }}" alt="RTT Fundraiser" class="img-responsive">
                </div>
                <div class="col-md-6 text-center">
                  <h2>Community Involvement</h2>
                  <p>Team BOE burning calories for cancer at Orange Theory Fitness. For every calorie burned The Real Teal, Inc., a 501(c)(3) nonprofit for Northeast Florida residents with ovarian cancer, received a donation from Bank of England Mortgage.</p>
                </div>
              </div>
            </div>
          </div>

        </div>
        <div class="container">
        <div class="row">
          <div class="col-lg-4 col-md-4 col-sm-6">
            <div class="thumbnail thumbnail-noBorder">
              <a href="http://www.unfospreys.com/" target="_blank"><img src="{{ url('images/community/north_florida_athletics_logo.png') }}" alt="North Florida Athletics"></a>
              <div class="caption partner-title">
                <p>Bank of England Mortgage Jacksonville is proud to be the Official Sponsor of <a href="http://www.unfospreys.com/" target="_blank">UNF Athletics</a>.</p>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-6">
            <div class="thumbnail thumbnail-noBorder">
              <a href="http://hungerfight.org/" target="_blank"><img src="{{ url('images/community/hunger_fight_logo.png') }}" alt="Hunger Fight"></a>
              <div class="caption partner-title">
                <p><a href="http://hungerfight.org/" target="_blank">Hunger Fight</a> – Distributing meals in local communities throughout the southeast to eradicate hunger and food insecurity.</p>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-6">
            <div class="thumbnail thumbnail-noBorder">
              <a href="http://therealteal.org/" target="_blank"><img src="{{ url('images/community/the_real_teal_logo.png') }}" alt="The Real Teal"></a>
              <div class="caption partner-title">
                <p><a href="http://therealteal.org/" target="_blank">The Real Teal</a> - BOE Mortgage Jacksonville is a proud sponsor of The Real Teal, a local nonprofit assisting ovarian cancer patients in Northeast Florida.</p>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-6">
            <div class="thumbnail thumbnail-noBorder">
              <a href="http://superservicechallenge.com/" target="_blank"><img src="{{ url('images/community/super_service_challenge.png') }}" alt="Super Service Challenge"></a>
              <div class="caption partner-title">
                <p><a href="http://superservicechallenge.com/" target="_blank">Super Service Challenge</a> encourages companies to volunteer together and in turn qualifies the nonprofit with additional funds and potential for a grand prize.</p>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-6">
            <div class="thumbnail thumbnail-noBorder">
              <a href="http://www.jaxbeachbaseball.org/" target="_blank"><img src="{{ url('images/community/jax_beach_baseball.jpg') }}" alt="JBBA"></a>
              <div class="caption partner-title">
                <p><a href="http://www.jaxbeachbaseball.org/" target="_blank">JBBA</a> - Coach of Bank of England Cardinals & JBBA Sponsor</p>
              </div>
            </div>
          </div>
        </div>
        <hr>
        <div class="row" style="margin-bottom: 40px">
          <h2 class="text-center"><i class="fa fa-phone-square" aria-hidden="true"></i> Important Numbers <small>Click on county to view phone numbers.</small></h2>
          <div class="col-md-4 text-center">
            <a href="#0" data-toggle="collapse" data-target="#nassau" aria-expanded="true" aria-controls="nassau"><h3>Nassau County</h3></a>
              <ul id="nassau" class="collapse" style="list-style: none; padding: 0; margin: 0">
                <li><a href="https://www.fpl.com/eccr/residential.html?cid=fpleccr2015gg011&utm_source=google&utm_medium=cpc&utm_term=florida%20electrical&utm_content=46660877139&utm_campaign=" target="_blank">Florida Power and Light (FPL)</a> <br> (800) 226-3545</li>
                <li><a href="http://www.usps.com/umove" target="_blank">USPS Change of Address</a></li>
                <li><a href="http://www.flhsmv.gov/offices/nassau.html">Department of Motor Vehicles</a> <br>(904) 491-7400 | (904) 491-7415</li>
                <li><a href="http://www.nassauflpa.com/exemptions/homestead-exemption">Nassau County Property Appraiser (Homestead Exemption)</a><span> <br>(904) 491-7300</span></li>
                <li><a href="http://www.aapcc.org/">Posion Control</a> <br>(800) 222-1222</li>
                <li><a href="http://call811.com/">Call Before You Dig</a> <br>811</li>
                <li><a href="http://www.nassaucountyfl.com/index.aspx?nid=144">Animal Care & Control</a> <br>(904) 530-6150</li>
              </ul>
          </div>
          <div class="col-md-4 text-center">
            <a href="#0" data-toggle="collapse" data-target="#duval" aria-expanded="true" aria-controls="duval"><h3>Duval County</h3></a>
              <ul id="duval" class="collapse" style="list-style: none; padding: 0; margin: 0">
                <li><a href="https://www.jea.com/" target="_blank">Jacksonville Electric Authority (JEA)</a> <br>(904) 665-6000</li>
                <li><a href="http://www.usps.com/umove" target="_blank">USPS Change of Address</a></li>
                <li><a href="http://www.flhsmv.gov/offices/duval.html">Department of Motor Vehicles</a> <br>(904) 630-1916</li>
                <li><a href="https://duval.fl.bhahomestead.com/public/">Duval County Property Appraiser (Homestead Exemption)</a><span> <br>(904) 630-2525</span></li>
                <li><a href="http://www.aapcc.org/">Posion Control</a> <br>(800) 222-1222</li>
                <li><a href="http://call811.com/">Call Before You Dig</a> <br>811</li>
                <li><a href="http://www.coj.net/departments/environmental-and-compliance/animal-care---protective-services.aspx">Animal Care & Control</a> <br>(904) 630-2489</li>
              </ul>
          </div>
          <div class="col-md-4 text-center">
            <a href="#0" data-toggle="collapse" data-target="#stjohns" aria-expanded="true" aria-controls="stjohns"><h3>St. Johns County</h3></a>
              <ul id="stjohns" class="collapse" style="list-style: none; padding: 0; margin: 0">
                <li><a href="https://www.fpl.com/eccr/residential.html?cid=fpleccr2015gg011&utm_source=google&utm_medium=cpc&utm_term=florida%20electrical&utm_content=46660877139&utm_campaign=">Florida Power and Light (FPL)</a> <br>(800) 226-3545 </li>
                <li><a href="https://www.jea.com/" target="_blank">Jacksonville Electric Authority (JEA) <small>services northern part of county</small></a> <br>(904) 665-6000</li>
                <li><a href="http://www.beachesenergy.com/">Beaches Energy Services <small>services Ponte Vedra</small></a> <br> (904) 247-6241</li>
                <li><a href="http://www.usps.com/umove" target="_blank">USPS Change of Address</a></li>
                <li><a href="http://www.flhsmv.gov/offices/stjohns.html">Department of Motor Vehicles</a> <br>(904) 209-2250</li>
                <li><a href="https://stjohns.fl.bhahomestead.com/ApplyOnline/WebForm1.aspx">St Johns County Property Appraiser (Homestead Exemption)</a><span> <br>(904) 827-5500</span></li>
                <li><a href="http://www.aapcc.org/">Posion Control</a> <br>(800) 222-1222</li>
                <li><a href="http://call811.com/">Call Before You Dig</a> <br>811</li>
                <li><a href="http://www.co.st-johns.fl.us/AnimalControl/index.aspx/">St. Johns County Division of Animal Control</a> <br> (904) 209-6190</li>
              </ul>
          </div>
        </div>
      </div>


@stop
