<div class="container">
  <!-- Static navbar -->
  <nav class="navbar navbar-custom navbar-default navbar-fixed-top">
    <div class="container">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="/">
          <img src="{{ url('images/site/boe_logo2.png') }}" alt="Bank of England Mortgage Jacksonville">
        </a>
      </div>
      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav navbar-right">
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-university" aria-hidden="true"></i> About Us <span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li><a href="/about/who-we-are">Who We Are</a></li>
              <li><a href="/#branches">Branches</a></li>
              <li><a href="/testimonials">Testimonials</a></li>
              <li><a href="/about/partners">Our Partners</a></li>
              <li><a href="/about/recognition">Recognitions</a></li>
              <li><a href="/community">Our Community</a></li>
              <li><a href="/videos">Videos</a></li>
              <li><a href="/about/careers">Careers</a></li>
            </ul>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-server" aria-hidden="true"></i> Loan Programs <span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="/loan-process">Loan Process</a></li>
                <li role="separator" class="divider"></li>
                <li><a href="/products/conventional">Conventional</a></li>
                <li><a href="/products/fha-loans">FHA Loans</a></li>
                <li><a href="/products/va-loans">VA Loans</a></li>
                <li><a href="/products/renovation">Renovation</a></li>
                <li><a href="/products/usda-loans">USDA Loans</a></li>
                <li><a href="/products/reverse-mortgage">Reverse Mortgage</a></li>
                <li><a href="/products/jumbo-loans">Jumbo Loans</a></li>
                <li><a href="/products/vacation">Vacation/Second Home Mortgage</a></li>
                <li><a href="/products/bond-loans">Bond Loans</a></li>
                <li><a href="/products/bridge-loans">Bridge Loans</a></li>
              </ul>
          </li>
          <li><a href="/apply" class="orange-highlight"><i class="fa fa-file-text" aria-hidden="true"></i> Apply Now</a></li>
          <li><a href="/about/branch/1114"><i class="fa fa-users" aria-hidden="true"></i> Our Team</a></li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-cog" aria-hidden="true"></i> Tools <span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li><a tabindex="-1" href="/tools/fastapp">BOE FastApp</a></li>
              <li><a tabindex="-1" href="/tools/glossary">Mortgage Glossary</a></li>
              <li><a tabindex="-1" href="/tools/mortgage-calculator">Mortgage Calculator</a></li>
              <li><a tabindex="-1" href="/tools/mortgage-calculator#affordability">Affordability Calculator</a></li>
              <li><a tabindex="-1" href="/tools/mortgage-calculator#rent-vs-buy">Rent vs Buy Calculator</a></li>
            </ul>
          </li>
            @if (isset($header_contact))
              <li><a href="/#contact"><i class="fa fa-inbox" aria-hidden="true"></i> Contact Us</a></li>
            @else
              <li><a href="#contact"><i class="fa fa-inbox" aria-hidden="true"></i> Contact Us</a></li>
            @endif
          
          <li><a href="https://www.facebook.com/thebankofengland/" target="_blank"><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
          <li><a href="https://www.twitter.com/boejax/" target="_blank"><i class="fa fa-twitter-square" aria-hidden="true"></i></a></li>
        </ul>
      </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
  </nav>
</div> <!-- /container -->
