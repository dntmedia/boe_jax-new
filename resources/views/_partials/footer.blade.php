@if (!isset($extended_footer))
  @include('_partials.full_contact')
@endif

<!-- Footer -->
<footer class="footer">
  <div class="container">
    <div class="row">
      <div class="col-sm-9 terms">
        <p>&copy 2017 BOE Mortgage. All Rights Reserved.</p>
        <ul class="list-inline">
          <li><a href="{{ url('assets/PDFs/Privacy-Policy-rev-082015.pdf') }}">Privacy Policy</a></li>
          <li><a href="/legal/analytics">Website Analytics Policy</a></li>
          <li><a href="/legal/security">Security</a></li>
          <li><a href="/legal/identity_theft">Preventing Identity Theft</a></li>
          <li><a href="/legal/patriot_act">USA Patriot Act</a></li>
          <li><a href="/legal/counseling">Need Credit Counseling</a></li>
          <li><a href="/about/careers">Careers</a></li>
        </ul>
        <p>Bank of England Mortgage has tried to provide accurate and timely information; however, the content of this site may not be accurate, complete or current and may include technical inaccuracies or typographical errors. From time to time changes may be made to the content of this site without notice. Bank of England Mortgage may change the products, services, and any other information described on this site at any time. The information published on this site is provided as a convenience to visitors and is for informational purposes only. You should verify all information before relying on it and decisions based on information contained in our site are your sole responsibility. If you are an individual with disabilities who needs accommodation, or you are having difficulty using our website to apply for a loan, please contact us at (904) 992-1000. This contact information is for accommodation requests only.</p>
        <p>Bank of England Mortgage is an equal opportunity employer and encourages women, minorities, persons with disabilities, and veterans to apply.</p>
      </div>
      <div class="col-sm-3">
        <p>Credit Bureau:</p>
        <ul class="list-inline terms">
          <li><a href="http://www.experian.com/" target="_blank">Experian</a></li>
          <li><a href="http://www.equifax.com/home/en_us" target="_blank">Equifax</a></li>
          <li><a href="http://www.transunion.com/" target="_blank">TransUnion</a></li>
        </ul>
        <div class="footer-content">
          <div class="col-sm-6 footer-icon-right">
            <a href="http://www.bbb.org/arkansas/business-reviews/mortgage-bankers/eng-lending-in-little-rock-ar-11000976" target="_blank"><img src="{{ url('images/site/_footer/footer-bbb.png') }}" class="img-responsive" alt="BBB Accredited Business logo" id="bbb-logo"></a>
          </div>
          <div class="col-sm-6">
            <div id="equal-house"><img src="{{ url('images/site/_footer/equalhousinglog.png') }}" class="img-responsive" alt="Equal Housing Lender logo"></div>
          </div>
        </div>
      </div>
      <div class="col-sm-9 terms">
        <p><a href="https://www.boemortgage.com/" target="_blank">Bank of England Mortgage</a> is a division of the <a href="https://www.bankofengland-ar.com/" target="_blank">Bank of England</a>. NMLS 418481. Bank of England Mortgage is not affiliated with any government agency. <a href="https://research.fdic.gov/bankfind/detail.html?bank=13303&name=Bank+of+England&tabId=3&searchName=Bank+of+England" target="_blank">Member FDIC.</a> </p>
      </div>
      <div class="col-sm-3 terms">
        <a href="http://www.nmlsconsumeraccess.org/">nmlsconsumeraccess.org</a>
      </div>
    </div>
  </div>
</footer>
