<section id="contact" class="contact-us">
  <div class="container">
    <div class="row">
      <div class="col-md-5">
        <img src="{{ url('images/site/_footer/boe_logo_light_blue.png') }}" alt="Bank of England Mortgage Jacksonville">
        <p>Bank of England Mortgage is a division of the Bank of England, a locally owned community bank located in England, Arkansas. We provide our clients with the expertise and services that are traditionally offered by the largest financial services institutions in the country - with the integrity of a local community bank.</p>
        <div class="col-xs-6" style="padding: 0">
          <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i> 13901 Sutton Park Drive S. Suite 170, Jacksonville, FL 32224</a>
        </div>
        <div class="col-xs-6" style="padding: 0">
          <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i> 13901 Sutton Park Drive S. Suite 403, Jacksonville, FL 32224</a>
        </div>
        <div class="row">
          <div class="col-xs-8 col-xs-offset-2" style="margin-top: 8px">
            <i class="fa fa-phone-square" aria-hidden="true"></i> Tel: (904) 992-1000 | 
            <i class="fa fa-fax" aria-hidden="true"></i>Fax: (904) 212-1061
          </div>
        </div>
      </div>
      <div class="col-md-7">
		    @include('_partials.contact_only')
      </div>
    </div><!-- ./row -->
  </div><!-- ./container -->
</section>
