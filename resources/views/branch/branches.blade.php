@extends('_layouts.app')

@section('keywords')
keywords
@stop
@section('description')
description
@stop


{{-- Web site Title --}}
@section('title')
Branches @parent
@stop

@section('styles')
@stop

@section('scripts')
@stop

@section('inline-scripts')
@stop


{{-- Content --}}
@section('content')



<section id="about" style="margin-top: 95px">
    <img src="{{ url('images/home/jax_team.jpg') }}" alt="Bank of England Mortgage Jacksonville" class="img-responsive">
    <div class="container branchpage">
      <h1 class="text-center">Bank of England Mortgage Jacksonville Branches</h1>
        <div class="row text-center">
          
          <div class="col-lg-6">
            <h3>
            <a href="/about/branch/1114">
              <address>
                <i class="fa fa-map-marker" aria-hidden="true"></i> 
                  13901 Sutton Park Drive S Suite 170
              </address>
              
            </a>
            </h3>
          </div>
          <div class="col-lg-6">
            <h3>
              <a href="/about/branch/1112">
                <address>
                  <i class="fa fa-map-marker" aria-hidden="true"></i> 
                    13901 Sutton Park Drive S Suite 403
                </address>
              </a>
            </h3>
          </div>

        </div><!-- end row -->

    </div><!-- end container -->

</section>

@stop
