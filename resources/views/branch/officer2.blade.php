@extends('_layouts.app')

@section('keywords')
keywords
@stop
@section('description')
description
@stop


{{-- Web site Title --}}
@section('title')
{{ $first_name }} {{ $last_name }} @parent
@stop

@section('styles')
  <link rel="stylesheet" type="text/css" href="{{ url('assets/css/lightslider.css') }}">
@stop

@section('scripts')
  <script type="text/javascript" src="{{ url('assets/vendors/lightslider/js/lightslider.min.js') }}"></script>
  <script type="text/javascript" src="{{ url('assets/js/light_slider.js') }}"></script>
@stop

@section('inline-scripts')
@stop


{{-- Content --}}
@section('content')



<section id="about" class="officer-section" style="margin-top: 95px;">
    <img class="img-responsive img-border-bottom" src="{{ url('images/employee/bio/' . $employee_info[0]->image_personal) }}" alt="Bank of England Mortgage Jacksonville, {{ $first_name }} {{ $last_name }}">
    <div class="container officer-content">
      <div class="row">
        <div class="col-md-3">
          <div class="thumbnail officer-thumbnail">
              <img src="{{ $photo }}" class="img-responsive" alt="{{ $first_name }} {{ $last_name }}">
              <div class="caption about-caption">
                <h3>{{ $job_title }}</h3>
                <h4>NMLS# {{ $nmls }}</h4>
                <h4>Office: {{ $office }} </h4>
                <h4>Mobile: {{ $mobile }}</h4>
                <h4>Fax: {{ $fax }} </h4>
                <h4><a href="mailto:{{ $email }}">{{ $email }}</a></h4> 
              </div>
            </div>

@if (!empty($leader_name))
            <p class="bioTeamName">Team: {{ $leader_name }} </p>
@endif

@if (!empty($content['team']))


<?php

$members = explode(',', $content['team']);

foreach ($members as $key => $member_id) {
    // if ($officer_id != $member_id) {

    $pathOrUrl = "https://www.boefastapp.net/V2/index.php?token=" . env('API_TOKEN') . "&action=show_member&id=" . $member_id;

    $json = file_get_contents($pathOrUrl);
    $content = json_decode($json, true);

    $data = call_user_func_array('array_merge', $content);

    echo '<div class="bioTeamColumn">';
    echo '<a href="' . $member_id . '">';
    echo '<img src="' . $data['photo'] . '" alt="' . $data['fname'] . $data['lname'] . '" class="bioTeamPic">';
    echo '</a>';
    echo '<br>';
    echo '<h3>';
    echo $data['fname'] . ' ' . $data['lname'];
    echo '</h3>';
    echo $data['title'];
    echo '<br>';
    echo 'NMLS# ' . $data['nmls'];
    echo '</div>';

    // }
}
?>


@endif





        </div>
        <div class="col-md-7">
          <div class="col-md-12">
            <h2>Meet {{ $first_name }} {{ $last_name }}</h2>
              {!! $biography !!}

            @if (!empty($employee_info[0]->wufoo_id))
                <!-- Button trigger modal -->
                <button type="button" class="btn main-btn" data-toggle="modal" data-target="#myModal">
                  Apply Now For A Loan
                </button>
            @endif

            @if (count($testimonials) > 0)
              <hr>
              <h3 class="margin-bottom-xl">Testimonials</h3>
              <ul id="vertical">
              @foreach($testimonials as $testimonial)
              <li>
              <div class="bioTestimonial">
                <div>
                  <em>&ldquo;{{ $testimonial->content }}&rdquo;</em>
                </div>
                <div class="text-right">
                  ~ <em>{{ $testimonial->person }}</em>
                </div>
              </div>
                <br>
                <br>
              </li>
              @endforeach
            </ul>
            @endif
          </div>
        </div>
        <div class="col-md-2">          
          @if (!empty($employee_info[0]->home_guide_pdf))
            <h5 class="text-center">Home Buyers Guide</h5>
            <a href="{{ url('content/home_guide/' .  $employee_info[0]->home_guide_pdf) }}" target="_blank"><img src="{{ url('content/home_guide_image/' .  $employee_info[0]->home_guide_image) }}" class="img-responsive"></a>
          @endif

          @if (!empty($employee_info[0]->wufoo_id))
            <!-- Button trigger modal -->
            <button type="button" class="btn main-btn" data-toggle="modal" data-target="#myModal">
              Apply Now For A Loan
            </button>
          @endif
        </div>
      </div><!-- end row -->

      <div style="margin: 20px 0 20px 20px">
        <a href="javascript: history.go(-1)" class="btn main-btn"><i class="fa fa-arrow-circle-o-left" aria-hidden="true"></i> Back to your mortgage team</a>
      </div>
    </div><!-- end container -->
</section>

    @if (!empty($employee_info[0]->wufoo_id))
    <!-- Modal -->
    <div class="modal fade bs-example-modal-lg" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">{{ $first_name }} {{ $last_name }}</h4>
          </div>
          <div class="modal-body">
            <div id="wufoo-{{ $employee_info[0]->wufoo_id }}">
            Fill out my <a href="https://boejax.wufoo.com/forms/{{ $employee_info[0]->wufoo_id }}">online form</a>.
            </div>
            <script type="text/javascript">var {{ $employee_info[0]->wufoo_id }};(function(d, t) {
            var s = d.createElement(t), options = {
            'userName':'boejax',
            'formHash':'{{ $employee_info[0]->wufoo_id }}',
            'autoResize':false,
            'height':'2289',
            'async':true,
            'host':'wufoo.com',
            'header':'show',
            'ssl':true};
            s.src = ('https:' == d.location.protocol ? 'https://' : 'http://') + 'www.wufoo.com/scripts/embed/form.js';
            s.onload = s.onreadystatechange = function() {
            var rs = this.readyState; if (rs) if (rs != 'complete') if (rs != 'loaded') return;
            try { {{ $employee_info[0]->wufoo_id }} = new WufooForm();{{ $employee_info[0]->wufoo_id }}.initialize(options);{{ $employee_info[0]->wufoo_id }}.display(); } catch (e) {}};
            var scr = d.getElementsByTagName(t)[0], par = scr.parentNode; par.insertBefore(s, scr);
            })(document, 'script');</script>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>
    @endif

@stop
