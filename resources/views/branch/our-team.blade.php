@extends('_layouts.app')

@section('keywords')
keywords
@stop
@section('description')
description
@stop


{{-- Web site Title --}}
@section('title')
Our Team @parent
@stop

@section('styles')
@stop

@section('scripts')
@stop

@section('inline-scripts')
@stop


{{-- Content --}}
@section('content')



<section id="about" style="margin-top: 95px">
    <img src="{{ url('images/home/jax_team.jpg') }}" alt="Bank of England Mortgage Jacksonville" class="img-responsive">
    <div class="container officer-content">
      <h1 class="text-center" style="margin-bottom: 40px">BOE Jax Team</h1>
        <div class="row">

          <h2>Location A</h2>

          @foreach($officers as $officer)
          <div class="col-lg-3 col-md-4 col-sm-6">
            <div class="thumbnail team-thumbnail" style="min-height: 610px">
            <a href="/about/our-team/{{ $officer['id'] }}"><img src="{{ $officer['photo']  }}" alt="{{ $officer['fname'] }} {{ $officer['lname'] }}"></a>
            <div class="caption about-caption">
              <h3>{{ $officer['fname'] }} {{ $officer['lname'] }}</h3>
              <h4>{{ $officer['title'] }}</h4>
              <p>NMLS # {{ $officer['nmls'] }}</p>
              <p>Office: {{ $officer['phone'] }}</p>
              <p>Mobile: {{ $officer['mobile'] }}</p>
              <p>Fax: {{ $officer['fax'] }}</p>
              <p><a href="{{ $officer['email'] }}">{{ $officer['email'] }}</a></p>
              <p>
                <a href="/about/our-team/{{ $officer['id'] }}" class="btn main-btn">Biography</a>
                <a href="/apply" class="btn main-btn">Apply</a>
              </p>

            </div>
            </div>
          </div>
          @endforeach

        </div><!-- end row -->

        @if ($officers2 != null)

        <hr>

        <div class="row">

          <h2>Location B</h2>

          @foreach($officers2 as $officer)
          <div class="col-lg-3 col-md-4 col-sm-6">
            <div class="thumbnail team-thumbnail" style="border: 1px solid black; min-height: 600px">
            <a href="/about/our-team/{{ $officer['id'] }}"><img src="{{ $officer['photo']  }}" alt="{{ $officer['fname'] }} {{ $officer['lname'] }}"></a>
            <div class="caption about-caption">
              <h3>{{ $officer['fname'] }} {{ $officer['lname'] }}</h3>
              <h4>{{ $officer['title'] }}</h4>
              <p>NMLS # {{ $officer['nmls'] }}</p>
              <p>Office: {{ $officer['phone'] }}</p>
              <p>Mobile: {{ $officer['mobile'] }}</p>
              <p>Fax: {{ $officer['fax'] }}</p>
              <p><a href="{{ $officer['email'] }}">{{ $officer['email'] }}</a></p>
              <p>
                <a href="/about/our-team/{{ $officer['id'] }}" class="btn main-btn">Biography</a>
                <a href="/apply" class="btn main-btn">Apply</a>
              </p>

            </div>
            </div>
          </div>
          @endforeach

        </div>

      @endif

    </div><!-- end container -->

</section>

@stop
