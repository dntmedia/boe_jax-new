@extends('_layouts.app')

@section('keywords')
keywords
@stop
@section('description')
description
@stop


{{-- Web site Title --}}
@section('title')
Our Team @parent
@stop

@section('styles')
  <link rel="stylesheet" type="text/css" href="{{ url('assets/css/tooltip.css') }}">
@stop

@section('scripts')
@stop

@section('inline-scripts')
@stop


{{-- Content --}}
@section('content')



<section id="about" style="margin-top: 95px">

  @if ( $id == 1114)
    <img src="{{ url('images/home/xmas_2016_1.jpg') }}" alt="Bank of England Mortgage Jacksonville" class="img-responsive">
  @else
    <img src="{{ url('images/home/jax_team.jpg') }}" alt="Bank of England Mortgage Jacksonville" class="img-responsive">
  @endif

    <div class="container officer-content">
      <h1 class="text-center" style="margin-bottom: 40px">{{ $branch_address }}</h1>
        <div class="row">

          @foreach($officers as $officer)
          @if ($officer['title'] == "Area Manager")
          <div class="col-lg-3 col-md-4 col-sm-6">
            <div class="thumbnail team-thumbnail">
            <a href="/about/our-team/{{ $officer['id'] }}"><img src="{{ $officer['photo']  }}" alt="{{ $officer['fname'] }} {{ $officer['lname'] }}"></a>
            <div class="caption about-caption">
              <h3>{{ $officer['fname'] }} {{ $officer['lname'] }}</h3>
              <h4>{{ $officer['title'] }}</h4>
              <h4>{{ $officer['team'] }}</h4>
              <p>NMLS # {{ $officer['nmls'] }}</p>
              <p>Office: {{ $officer['phone'] }}</p>
              <p>Mobile: {{ $officer['mobile'] }}</p>
              <p>Fax: {{ $officer['fax'] }}</p>
              <p><a href="{{ $officer['email'] }}">{{ $officer['email'] }}</a></p>
              <p>
                <a href="/about/our-team/{{ $officer['id'] }}" class="btn main-btn">Biography</a>
                <a href="/apply" class="btn main-btn">Apply</a>
              </p>

            </div>
            </div>
          </div>
          @endif
          @endforeach
          @foreach($officers as $officer)
          @if ($officer['title'] != "Area Manager")
          <div class="col-lg-3 col-md-4 col-sm-6">
            <div class="thumbnail team-thumbnail">

            <a href="/about/our-team/{{ $officer['id'] }}"><img src="{{ $officer['photo']  }}" alt="{{ $officer['fname'] }} {{ $officer['lname'] }}">
        <?php
          if (!empty($officer['team'])) {
              $members = explode(',', $officer['team']);

              $leader_id = reset($members);
              $pathOrUrl = "https://www.boefastapp.net/V2/index.php?token=" . env('API_TOKEN') . "&action=show_member&id=" . $leader_id;
              $json = file_get_contents($pathOrUrl);
              $leader = json_decode($json, true);
              // print_r($leader);

              ?>
        <div class="tool-tip slideIn bottom">

          <h3 class="text-center">
            {{ $leader[0]['fname'] }} {{ $leader[0]['lname'] }} Team
            <br>
          </h3>

        <?php
          foreach ($members as $key => $value) {
                  $pathOrUrl = "https://www.boefastapp.net/V2/index.php?token=" . env('API_TOKEN') . "&action=show_member&id=" . $value;
                  $json = file_get_contents($pathOrUrl);
                  $content = json_decode($json, true);
                  ?>
                      
                        <div class="col-xs-3">
                          <a href="/about/our-team/{{ $value }}"><img src="{{ $content[0]['photo']  }}" alt="{{ $content[0]['fname'] }} {{ $content[0]['lname'] }}"></a>

                          <div class="caption teamPopupCaption">
                            <h4>{{ $content[0]['fname'] }} {{ $content[0]['lname'] }}</h4>
                            <p>{{ $content[0]['title'] }}</p>
                            <p>NMLS # {{ $content[0]['nmls'] }}</p>
                            <p>Office: {{ $content[0]['phone'] }}</p>
                            <p>Mobile: {{ $content[0]['mobile'] }}</p>
                            <p>Fax: {{ $content[0]['fax'] }}</p>
                            <p><a href="{{ $content[0]['email'] }}">{{ $content[0]['email'] }}</a></p>
                          </div>

                        </div>

                        <?php
                          }
                              ?>
                                           </div>
                                          <?php
                          }
                          ?>
                      </a>
                <div class="caption about-caption">
                  <h3>{{ $officer['fname'] }} {{ $officer['lname'] }}</h3>
                  <h4>{{ $officer['title'] }}</h4>
                  <p>NMLS # {{ $officer['nmls'] }}</p>
                  <p>Office: {{ $officer['phone'] }}</p>
                  <p>Mobile: {{ $officer['mobile'] }}</p>
                  <p>Fax: {{ $officer['fax'] }}</p>
                  <p><a href="{{ $officer['email'] }}">{{ $officer['email'] }}</a></p>
                  <p>
                    <a href="/about/our-team/{{ $officer['id'] }}" class="btn main-btn">Biography</a>
                    <a href="/apply" class="btn main-btn">Apply</a>
                  </p>
                </div>
              </div>
          </div>
          @endif
          @endforeach

        </div><!-- end row -->

    </div><!-- end container -->

</section>

@stop
