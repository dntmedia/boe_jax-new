@extends('_layouts.app')

@section('keywords')
keywords
@stop
@section('description')
description
@stop


{{-- Web site Title --}}
@section('title')
{{ $first_name }} {{ $last_name }} @parent
@stop

@section('styles')
  <link rel="stylesheet" type="text/css" href="{{ url('assets/css/lightslider.css') }}">
@stop

@section('scripts')
  <script type="text/javascript" src="{{ url('assets/vendors/lightslider/js/lightslider.min.js') }}"></script>
  <script type="text/javascript" src="{{ url('assets/js/light_slider.js') }}"></script>
@stop

@section('inline-scripts')
@stop


{{-- Content --}}
@section('content')



<section id="about" class="officer-section" style="margin-top: 95px;">
    <img class="img-responsive img-border-bottom" src="{{ url('images/employee/bio/' . $employee_info[0]->image_personal) }}" alt="Bank of England Mortgage Jacksonville, {{ $first_name }} {{ $last_name }}">
    <div class="container officer-content">
      <div class="row">
        <div class="col-md-3">
          <div class="thumbnail officer-thumbnail">
              <img src="{{ $photo }}" class="img-responsive" alt="{{ $first_name }} {{ $last_name }}">
              <div class="caption about-caption">
                <h3>{{ $job_title }}</h3>
                @if ( $nmls != "")
                  <h4>NMLS# {{ $nmls }}</h4>
                @else

                @endif
                <h4>Office: {{ $office }} </h4>
                @if ( $mobile != "")
                  <h4>Mobile: {{ $mobile }}</h4>
                @else
                  
                @endif

                @if ( $fax != "")
                  <h4>Fax: {{ $fax }} </h4>
                @else
                @endif
                <h4><a href="mailto:{{ $email }}">{{ $email }}</a></h4> 
                <p><a href="{{ $apply_link }}" target="_blank" class="btn main-btn">Apply Now</a></p>
              </div>
            </div>

@if (!empty($leader_name))
          @if (($leader_name === 'Tatum') || ($leader_name === 'Thors'))
            <p class="bioTeamName">{{ $leader_name }} Home Team</p>
          @elseif ($leader_name === 'Bladen')
            <p class="bioTeamName">Marketing Team</p>
          @else
            <p class="bioTeamName">{{ $leader_name }} Team</p>
          @endif
@endif

<div class="bioTeamMembers">
@if (!empty($content['team']))
<?php
$members = explode(',', $content['team']);

foreach ($members as $key => $member_id) {
    // if ($officer_id != $member_id) {

    $pathOrUrl = "https://www.boefastapp.net/V2/index.php?token=" . env('API_TOKEN') . "&action=show_member&id=" . $member_id;

    $json = file_get_contents($pathOrUrl);
    $content_extra = json_decode($json, true);

    $data = call_user_func_array('array_merge', $content_extra);

    echo '<div class="bioTeamColumnDesktop bioTeamColumn">';
    echo '<a href="' . $member_id . '">';
    echo '<img src="' . $data['photo'] . '" alt="' . $data['fname'] . $data['lname'] . '" class="bioTeamPic">';
    echo '</a>';
    echo '<br>';
    echo '<h3>';
    echo $data['fname'] . ' ' . $data['lname'];
    echo '</h3>';
    echo $data['title'];
    echo '<br>';
    echo 'NMLS# ' . $data['nmls'];
    echo '</div>';
    // }
}
?>


@endif
</div>

<div class="clearfix"></div>
<br>
<br>
@if (!empty($leader_name_2))
          @if (($leader_name_2 === 'Tatum') || ($leader_name_2 === 'Thors'))
            <p class="bioTeamName">{{ $leader_name_2 }} Home Team</p>
          @elseif ($leader_name_2 === 'Bladen')
            <p class="bioTeamName">Marketing Team</p>
          @else
            <p class="bioTeamName">{{ $leader_name_2 }} Team</p>
          @endif
@endif

<div class="bioTeamMembers">
@if (!empty($content['team_2']))
<?php
$members = explode(',', $content['team_2']);

foreach ($members as $key => $member_id) {
    // if ($officer_id != $member_id) {

    $pathOrUrl = "https://www.boefastapp.net/V2/index.php?token=" . env('API_TOKEN') . "&action=show_member&id=" . $member_id;

    $json = file_get_contents($pathOrUrl);
    $content = json_decode($json, true);

    $data = call_user_func_array('array_merge', $content);

    echo '<div class="bioTeamColumnDesktop bioTeamColumn">';
    echo '<a href="' . $member_id . '">';
    echo '<img src="' . $data['photo'] . '" alt="' . $data['fname'] . $data['lname'] . '" class="bioTeamPic">';
    echo '</a>';
    echo '<br>';
    echo '<h3>';
    echo $data['fname'] . ' ' . $data['lname'];
    echo '</h3>';
    echo $data['title'];
    echo '<br>';
    echo 'NMLS# ' . $data['nmls'];
    echo '</div>';
    // }
}
?>


@endif
</div>
<div class="clearfix"></div>
<br>
<br>
        </div>
        <div class="col-md-7">
          <div class="col-md-12">
            <h2>Meet {{ $first_name }} {{ $last_name }}</h2>
              {!! $biography !!}

            @if (count($testimonials) > 0)
              <hr>
              <h3 class="margin-bottom-xl">Testimonials</h3>
              <ul id="vertical">
              @foreach($testimonials as $testimonial)
              <li>
              <div class="bioTestimonial">
                <div>
                  <em>&ldquo;{{ $testimonial->content }}&rdquo;</em>
                </div>
                <div class="text-right">
                  ~ <em>{{ $testimonial->person }}</em>
                </div>
              </div>
                <br>
                <br>
              </li>
              @endforeach
            </ul>
            @endif
          </div>
        </div>
        <div class="col-md-2">          
          @if (!empty($employee_info[0]->home_guide_pdf))
            <h5 class="text-center">Home Buyers Guide</h5>
            <a href="{{ url('content/home_guide/' .  $employee_info[0]->home_guide_pdf) }}" target="_blank"><img src="{{ url('content/home_guide_image/' .  $employee_info[0]->home_guide_image) }}" class="img-responsive"></a>
          @endif

        </div>
      </div><!-- end row -->

      <div style="margin: 20px 0 20px 20px">
        <a href="javascript: history.go(-1)" class="btn main-btn"><i class="fa fa-arrow-circle-o-left" aria-hidden="true"></i> Back to your mortgage team</a>
      </div>
    </div><!-- end container -->
</section>

@stop
