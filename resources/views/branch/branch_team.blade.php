@extends('_layouts.app')

@section('keywords')
keywords
@stop
@section('description')
description
@stop


{{-- Web site Title --}}
@section('title')
Our Team @parent
@stop

@section('styles')
@stop

@section('scripts')
@stop

@section('inline-scripts')
$(document).ready(function() {

  $('.mobileBioPicHover').hover(function() {
    $(this).addClass('animated pulse').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', 
      function() {
        $(this).removeClass('animated pulse');
      });
  });
});
@stop


{{-- Content --}}
@section('content')



<section id="about" style="margin-top: 95px">

  @if ( $id == 1114)
    <img src="{{ url('images/home/xmas_2016_1.jpg') }}" alt="Bank of England Mortgage Jacksonville" class="img-responsive">
  @else
    <img src="{{ url('images/home/jax_team.jpg') }}" alt="Bank of England Mortgage Jacksonville" class="img-responsive">
  @endif

    <div class="container officer-content">
      <h1 class="text-center teamAddressHeading" style="margin-bottom: 40px">{{ $branch_address }}</h1>
        <div class="row">

          @foreach($officers as $officer)
          @if ($officer['title'] == "Area Manager")
          <div class="col-sm-6 col-md-4 col-lg-3 margin-bottom-xl margin-top-xl">

              <a href="/about/our-team/{{ $officer['id'] }}" class=""><img src="{{ $officer['photo']  }}" alt="{{ $officer['fname'] }} {{ $officer['lname'] }}" class="img-responsive mobileBioPic mobileBioPicHover"></a>

                <div class="caption about-caption">
                  <h3>{{ $officer['fname'] }} {{ $officer['lname'] }}</h3>
                  <h4>{{ $officer['title'] }}</h4>
                  <h4>{{ $officer['team'] }}</h4>

                  @if( $officer['nmls'] != "")
                    <h4>NMLS # {{ $officer['nmls'] }}</h4>
                  @else
                    <h4>&nbsp;</h4>
                  @endif

                </div>

          </div>
          @endif
          @endforeach


          @foreach($officers as $officer)
          @if ($officer['title'] != "Area Manager")
          <div class="col-sm-6 col-md-4 col-lg-3 margin-bottom-xl margin-top-xl">

              <a href="/about/our-team/{{ $officer['id'] }}" class=""><img src="{{ $officer['photo']  }}" alt="{{ $officer['fname'] }} {{ $officer['lname'] }}" class="img-responsive mobileBioPic mobileBioPicHover"></a>

                <div class="caption about-caption">
                  <h3>{{ $officer['fname'] }} {{ $officer['lname'] }}</h3>
                  <h4>{{ $officer['title'] }}</h4>

                  @if( $officer['nmls'] != "")
                    <h4>NMLS # {{ $officer['nmls'] }}</h4>
                  @else
                    <h4>&nbsp;</h4>
                  @endif
                    &nbsp;
                </div>


          </div>
          @endif
          @endforeach

        </div><!-- end row -->

    </div><!-- end container -->
</section>

@stop
