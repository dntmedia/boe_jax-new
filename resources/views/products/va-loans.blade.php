@extends('_layouts.app')

@section('keywords')
keywords
@stop
@section('description')
description
@stop


{{-- Web site Title --}}
@section('title')
VA Loans @parent
@stop

@section('styles')
@stop

@section('scripts')
@stop

@section('inline-scripts')
@stop


{{-- Content --}}
@section('content')
<section>
  <div class="container">
    <div class="row body-margin-top">
      <div class="col-md-12">
        <div class="product-content">
          <img src="{{ url('images/products/va_loan_icon_blue.png') }}" alt="Bank of England Mortgage VA Loans" class="fa img-responsive">
          <h1>VA Loans</h1>
        </div>
        <div class="who-we-are-body">
          <div class="product-content">
            <div>
              <p>Here you'll find long-term insurance-free VA home loans that are guaranteed by the United States Veterans Administration.</p>
              <p>Bank of England Mortgage offers affordable, easy-to-understand VA Home loans in recognition of the contributions and sacrifices veterans have made for America. Whether you're buying your first home, refinancing your existing mortgage, or thinking of building your one-of-a-kind dream house, Bank of England Mortgage is your ultimate Veteran's Affairs loans information center.</p>
            </div>
            <img src="{{ url('images/products/va_loan.jpg')}}" class="img-responsive" alt="Bank of England Mortgage offers affordable, easy-to-understand VA Home loans in recognition of the contributions and sacrifices veterans have made for America. ">
          </div>

          <p>We'll compare VA home loan interest rates and negotiate the very best deal. We'll also explain important factors to consider, such as the option to prepay your VA mortgage loans without penalty, and how a lack of mortgage insurance premiums can lower your monthly payments. We can even show you how to get VA home loan funds to improve your home's energy efficiency!  <i>Did you know your VA benefits can be used again even if you used it in the past?</i></p>

          <p>Our highly trained professionals make applying for VA loans online a breeze! At Bank of England Mortgage you'll discover super low fixed-rate interest options, up to 100% financing, and you may not even need a down payment! Call us now and let us help you find the perfect VA home loan to suit your individual needs.</p>

          <p>Contact us today at <a href="telto:9049921000">(904) 992-1000</a> to get started or <a href="/apply">start an application today</a>.</p>

          <ul class="pager">
            <li class="previous"><a href="/products/fha-loans">Previous</a></li>
            <li class="next"><a href="/products/renovation">Next</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</section>

@stop
