@extends('_layouts.app')

@section('keywords')
keywords
@stop
@section('description')
description
@stop


{{-- Web site Title --}}
@section('title')
Conventional Loans @parent
@stop

@section('styles')
@stop

@section('scripts')
@stop

@section('inline-scripts')
@stop


{{-- Content --}}
@section('content')
<section>
  <div class="container">
    <div class="row body-margin-top">
      <div class="col-md-12">
        <div class="product-content">
          <img src="{{ url('images/products/conventional_loan_icon_blue.png') }}" alt="Bank of England Mortgage Conventional Loans" class="fa img-responsive">
          <h1> Conventional Loan</h1>
        </div>
        <div class="who-we-are-body">
          <div class="product-content">
            <p>Most simply stated, a conventional loan means a homebuyer’s mortgage is not backed or insured by a government agency such as the Federal Housing Administration (FHA) or Veterans Administration (VA). Buyers can use a conventional mortgage to purchase a one- to four-unit home, a condominium, modular or manufactured home as a primary, secondary or investment property.</p>
            <img src="{{ url('images/products/home_equity.jpg')}}" class="img-responsive" alt="Contact us to discuss more about conventional loans.">
          </div>
          <p>The down payment of a conventional purchase loan is generally higher than that of a government-insured loan, such as an FHA loan, which is 3.5%. Conventional borrowers generally must pay between 5% to 20% percent of a home’s purchase price for a down payment. On a refinance, a conventional loan can have a loan-to-value (LTV) ratio as high as 95% with the addition of private mortgage insurance (PMI) or lender paid mortgage insurance (LPMI).</p>
          <p>On a conventional loan, we will consider a buyer’s debt-to-income (DTI) ratio, which is determined by calculating the projected housing costs and actual recurring monthly expenses. A buyer’s home expenses include: monthly loan payment applied toward principal and interest, property taxes, mortgage and homeowners insurance in addition to all monthly expenditures, must not exceed 43% of the buyer’s income.</p>
          <p>A FICO credit score of 640 and higher increases a borrower’s rate of approval and may reduce the loan’s interest rate.</p>
          <p>Borrowers who have filed a Chapter 7 bankruptcy case can apply for a conventional mortgage after four years from discharge date, and those who have filed a Chapter 13 may apply two years after the re-establishment of an active credit profile.</p>
          <p>The current market and the borrower’s FICO credit score influence the interest rate he’ll receive on a conventional loan. Conventional loan programs at a fixed rate, where the interest rate stays consistently the same throughout the term of the loan, or as an ARM, an adjustable-rate mortgage, where interest rates initiate at a below-market rate and change on a designated schedule, which ranges from monthly to annually or longer.</p>
          <p>Conforming conventional loans have a maximum loan limit set by Fannie Mae at the county level. In the case of non-conforming loans, banks generally set the limit at 80 to 90 percent of the home’s appraised value.</p>
          <p>Need more information? Contact us today at <a href="telto:9049921000">(904) 992-1000</a> to get started or <a href="/apply">start an application today</a>.</p>

          <ul class="pager">
            <li class="previous"><a href="/products/bridge-loans">Previous</a></li>
            <li class="next"><a href="/products/fha-loans">Next</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</section>

@stop
