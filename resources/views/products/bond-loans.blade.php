@extends('_layouts.app')

@section('keywords')
keywords
@stop
@section('description')
description
@stop


{{-- Web site Title --}}
@section('title')
Bond Loans @parent
@stop

@section('styles')
@stop

@section('scripts')
@stop

@section('inline-scripts')
@stop


{{-- Content --}}
@section('content')
<section>
  <div class="container">
    <div class="row body-margin-top">
      <div class="col-md-12">
        <div class="product-content">
          <img src="{{ url('images/products/bond_loan_icon_blue.png') }}" alt="Bank of England Mortgage Bond Loans" class="fa img-responsive">
          <h1>Bond Loans</h1>
        </div>
      <div class="who-we-are-body">
        <div class="product-content">
          <div>
            <p>The state or local government sells mortgage revenue bonds. The funds raised from this provide the funds for this second mortgage which assist in subsidizing costs associated with the mortgage loan under the Hardest Hit Fund.</p>
            <p>When you purchase a home using a bond loan, the bond loan acts as a second mortgage. Eligibility, requirements and other details regarding this loan vary by county. If eligible, a bond loan is available on Conventional, VA and FHA loans. Eligible borrowers may receive up to $15,000 in some counties. Borrowers who have not owned property in the past 3 years may be eligible.</p>
          </div>
          <img src="{{ url('images/products/bond_loans.jpg')}}" class="img-responsive" alt="">
        </div>

        <p>To begin determining eligibility, contact us today at<a href="telto:9049921000"> (904) 992-1000</a> to get started or <a href="/apply">start an application today</a>.</p>

        <ul class="pager">
          <li class="previous"><a href="/products/vacation">Previous</a></li>
          <li class="next"><a href="/products/bridge-loans">Next</a></li>
        </ul>
      </div>
    </div>
  </div>
</section>

@stop
