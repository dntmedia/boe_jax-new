@extends('_layouts.app')

@section('keywords')
keywords
@stop
@section('description')
description
@stop


{{-- Web site Title --}}
@section('title')
USDA Loans @parent
@stop

@section('styles')
@stop

@section('scripts')
@stop

@section('inline-scripts')
@stop


{{-- Content --}}
@section('content')
<section>
  <div class="container">
    <div class="row body-margin-top">
      <div class="col-md-12">
        <div class="product-content">
          <img src="{{ url('images/products/usda_loan_icon_blue.png') }}"  alt="Bank of England Mortgage USDA Loans" class="fa img-responsive">
          <h1> USDA Loans</h1>
        </div>
        <div class="who-we-are-body">
          <div class="product-content">
            <p>A home loan from the USDA loan program, also known as the USDA Rural Development Guaranteed Housing Loan Program, is a mortgage loan offered to rural property owners by the United States Department of Agriculture.</p>
            <img src="{{ url('images/products/usda.jpg')}}" class="img-responsive" alt="A USDA home loan is different from a traditional mortgage offered in the United States in several ways. Contact BOE Mortgage branch for more information.">
          </div>

          <p>A USDA home loan is different from a traditional mortgage offered in the United States in several ways.</p>
          <ul>
            <li><p>USDA loans require no down payment, you may finance up to 100% of the property value.</p></li>
            <li><p>You must meet the income restrictions for the County you are interested in. Each county has a maximum Income Requirement. The USDA Home Loan Program does allow for considerations for expenses like Child Care.</p></li>
            <li><p>To be eligible, you must be purchasing a property in a rural area as defined by the USDA. Often times the eligible property is just outside city limits.</p></li>
            <li><p>The home or property that you are looking to purchase must be owner-occupied, investment properties are not eligible for USDA loans.</p></li>
            <li><p>Refinancing your current USDA loan is also available.</p></li>
          </ul>

          <hr>

          <h3>Type of USDA Loan</h3>

          <p style="font-weight: 800">Guranteed Loan</p>

          <div style="margin-left: 15px">
            <p>Applicants for home loans may have an income of up to 115% of the median income for the area. Families must be without adequate housing, but be able to afford the mortgage payments, including taxes and insurance. In addition, applicants must have reasonable credit histories. Additionally, the property must be located within the USDA RD Home Loan "footprint." The USDA Home Loan maps are currently scheduled to be changed on September 30, 2013.</p>
            <p>Approved lenders under the Single Family Housing Guaranteed Loan program include:</p>
            <p>Any State housing agency; Lenders approved by: HUD for submission of applications for Federal Housing Mortgage Insurance or as an issuer of Ginnie Mae mortgage backed securities; the U.S. Veterans Administration as a qualified mortgagee; Fannie Mae for participation in family mortgage loans; Freddie Mac for participation in family mortgage loans; Any FCS (Farm Credit System) institution with direct lending authority; Any lender participating in other USDA Rural Development and/or Farm Service Agency guaranteed loan programs.</p>
          </div>

          <hr>
          <p>Contact us today at <a href="telto:9049921000">(904) 992-1000</a> to get started or <a href="/apply">start an application today</a>.</p>

          <ul class="pager">
            <li class="previous"><a href="/products/renovation">Previous</a></li>
            <li class="next"><a href="/products/reverse-mortgage">Next</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</section>

@stop
