@extends('_layouts.app')

@section('keywords')
keywords
@stop
@section('description')
description
@stop


{{-- Web site Title --}}
@section('title')
Vacation/Second Home Mortgage @parent
@stop

@section('styles')
@stop

@section('scripts')
@stop

@section('inline-scripts')
@stop


{{-- Content --}}
@section('content')
<section>
  <div class="container">
    <div class="row body-margin-top">
      <div class="col-md-12">
        <div class="product-content">
          <img src="{{ url('images/products/vacation_loan_icon_blue.png') }}" alt="Bank of England Mortgage Vacation/Second Home Mortgage Loans" class="fa img-responsive">
          <h1>Vacation/Second Mortgage</h1>
        </div>
        <div class="who-we-are-body">
          <div class="product-content">
            <div>
              <p>Bank of England Mortgage's Vacation-Second Home Mortgages offer a broad array of fixed, adjustable, and interest-only options. We work together with our clients to identify the ideal mortgage for your vacation home.</p>
              <p>Get your Bank of England vacation home mortgage loan and make your dreams a reality - backyard barbeques and sunsets by the lake. Fun-filled relaxing fishing trips with children and grand children. Exciting family skiing vacations. A lifetime of memories.</p>
            </div>
            <img src="{{ url('images/products/vacation_second_home.jpg')}}" class="img-responsive" alt="Bank of England Mortgage's Vacation-Second Home Mortgages offer a broad array of fixed, adjustable, and interest-only options. ">
          </div>

          <p>It's time to buy your vacation home - and that means a vacation home mortgage loan from Bank of England Mortgage.</p>
          <p>If you're in need of a mortgage for a vacation home, we eliminate the hassles and the headaches from the process.</p>
          <hr>

          <h3>Why is it called a vacation home mortgage?</h3>
            <blockquote><p><em>We call it a vacation home mortgage to eliminate the confusion between a second mortgage and a home equity loan.</em></p></blockquote>
          <h3>What's the difference between a second mortgage and a home equity loan?</h3>
            <blockquote><p><em>A second mortgage loan covers the costs of a second home, for example, a vacation home. The result is that the borrower has two mortgages and two homes. A home equity loan, on the other hand, allows the borrower to refinance their existing mortgage to get cash - or equity - out of their home. It's still one mortgage, but it's been refinanced.</em></p></blockquote>
          <h3>Are interest-only vacation home mortgage rates available?</h3>
            <blockquote><p><em>Yes! You can choose to pay a fixed, interest-only rate for a period of 10 years or 15 years with our interest-only vacation home mortgage.</em></p></blockquote>
          <h3>How much money can I be approved for with a Vacation Second Home mortgage?</h3>
            <blockquote><p><em>Depending on your income and expenses, you may qualify for a loan of $417,000 to $1.5 million. we are committed to working with you to help make your vacation home mortgage dreams come true.</em></p></blockquote>

          <p>Contact us today at <a href="telto:9049921000">(904) 992-1000</a> to get started or <a href="/apply">start an application today</a>.</p>

          <ul class="pager">
            <li class="previous"><a href="/products/jumbo-loans">Previous</a></li>
            <li class="next"><a href="/products/bond-loans">Next</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</section>

@stop
