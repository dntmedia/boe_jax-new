@extends('_layouts.app')

@section('keywords')
keywords
@stop
@section('description')
description
@stop


{{-- Web site Title --}}
@section('title')
Jumbo Loans @parent
@stop

@section('styles')
@stop

@section('scripts')
@stop

@section('inline-scripts')
@stop


{{-- Content --}}
@section('content')
<section>
  <div class="container">
    <div class="row body-margin-top">
      <div class="col-md-12">
        <div class="product-content">
          <img src="{{ url('images/products/jumbo_loan_icon_blue.png') }}" alt="Bank of England Mortgage Jumbo Loans" class="fa img-responsive">
          <h1>Jumbo Loans</h1>
        </div>
        <div class="who-we-are-body">
          <div class="product-content">
            <p><strong>We Make Qualifying For A Large Loan Straightforward, Fast, Easy. </strong>At Bank of England Mortgage, our specialty is funding the dream of homeownership - no matter the size of the loan or dream - for that matter.</p>
            <img src="{{ url('images/products/jumbo_loan.jpg')}}" alt="Inquiry today about jumbo loans" class="img-responsive">
          </div>
          <p>That's why we created our jumbo mortgage lending program - a full suite of offerings designed with the needs of borrowers that seek larger loan amounts than were available through traditional lending options.</p>

          <p>It's as simple as it sounds. We call it a jumbo mortgage because it is larger than other more "typical" home mortgage loans.

          <h3>Why would I need a jumbo mortgage loan?</h3>
            <blockquote>
              <p><em>If you are seeking a loan amount in excess of $417,000, then you need a jumbo mortgage loan. Each year, Fannie Mae and Freddie Mac , two government sponsored enterprises, set loan maximums. When loan amounts exceed their limits - the borrower is purchasing a jumbo mortgage loan.</em></p>
            </blockquote>
          <h3>How much money can I be qualified for with a jumbo mortgage?</h3>
            <blockquote>
              <p><em>Depending on the borrower's financial profile, you can qualify for $417,000 to $1,500,000.</em></p>
            </blockquote>
          <h3>What kind of credit score do I need in order to qualify for either an interest-only jumbo mortgage or jumbo mortgage?</h3>
            <blockquote>
              <p><em>That depends on the type of loan. Credit scores for jumbo mortgages are similar to conventional credit score requirements.</em></p>
            </blockquote>
          <p>Our Loan Specialists are trained to work with you to determine if you qualify for a jumbo mortgage. To determine if a jumbo mortgage is right for you, please contact us today at <a href="telto:9049921000">(904) 992-1000</a> for more information.</p></p>

          <ul class="pager">
            <li class="previous"><a href="/products/reverse-mortgage">Previous</a></li>
            <li class="next"><a href="/products/vacation">Next</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</section>

@stop
