@extends('_layouts.app')

@section('keywords')
keywords
@stop
@section('description')
description
@stop


{{-- Web site Title --}}
@section('title')
Contact Us @parent
@stop

@section('styles')
@stop

@section('scripts')
  <script type="text/javascript" src="{{ url('assets/js/contact/validator.js') }}"></script>
@stop

@section('inline-scripts')
@stop


{{-- Content --}}
@section('content')
<div class="container">

    <section id="contact-banner">
      <div class="container">
        <div class="row">
          <div class="flex-content contact-content">
            <div class="col-lg-8 col-lg-offset-2">
              <h1>We can't wait to hear from you! Please complete the form below.</h1>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section id="contact" class="contact-us">
      <div class="container">

        @include('_partials.contact_only')

      </div>
    </section>

</div><!-- ./container -->
@stop
