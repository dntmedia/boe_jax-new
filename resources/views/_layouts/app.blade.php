<!DOCTYPE html>
<html lang="en">
<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<meta name="author" content="{!! env('AUTHOR', 'AUTHOR') !!}" />

	<meta name="keywords" content="@section('keywords')@show, {!! env('KEYWORDS') !!}">
	<meta name="description" content="@section('description')@show, {!! env('DESCRIPTION') !!}">

	<title>
		@section('title')
			{!! env('SITE_TITLE', 'TITLE') !!}
		@show
	</title>

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

	<link rel="shortcut icon" href="{{ url('ico/favicon.png') }}">
	<link rel="icon" href="{{ url('favicon.ico') }}">
	<link rel="apple-touch-icon-precomposed" sizes="57x57" href="{{ url('ico/apple-touch-icon-57-precomposed.png') }}">
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ url('ico/apple-touch-icon-72-precomposed.png') }}">
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{ url('ico/apple-touch-icon-144-precomposed.png') }}">

<!-- ------------------------------------------ Google Fonts ------------------------------------------ -->

	<link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Lobster" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Squada+One" rel="stylesheet">

<!-- ------------------------------------------ CSS stylesheets ------------------------------------------ -->

	<link rel="stylesheet" type="text/css" href="{{ url('assets/vendors/bootstrap-3.3.7-dist/css/bootstrap.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ url('assets/vendors/font-awesome-4.7.0/css/font-awesome.css') }}">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

<!--
	<link rel="stylesheet" type="text/css" href="{{ url('assets/vendors/bootstrap-3.3.5-dist/css/bootstrap-theme.min.css') }}">
-->

	<link rel="stylesheet" type="text/css" href="{{ url('assets/vendors/illuminate3/css/standard.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ url('assets/css/app.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ url('assets/css/site.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ url('assets/css/styles.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ url('assets/css/animate.css') }}">

<!-- ------------------------------------------ app loaded CSS stylesheets ------------------------------------------ -->
	@yield('styles')

<!-- ------------------------------------------ head loaded js ------------------------------------------ -->

</head>

<body class="Site">


<header class="Site-header">
	@include('_partials.header')
</header>

<main class="Site-main">
	@include('_partials.content')
</main>

<footer class="Site-footer">
	@include('_partials.footer')
</footer>


<!-- ------------------------------------------ js ------------------------------------------ -->

	<script type="text/javascript" src="{{ url('assets/vendors/jquery/jquery-1.11.1.min.js') }}"></script>
	<script type="text/javascript" src="{{ url('assets/vendors/jquery/jquery-1.11.1.min.map') }}"></script>
	<script type="text/javascript" src="{{ url('assets/vendors/bootstrap-3.3.7-dist/js/bootstrap.min.js') }}"></script>
	<!-- <script type="text/javascript" src="{{ url('assets/js/jquery-2.1.4.js') }}"></script> -->
	<!-- <script type="text/javascript" src="{{ url('assets/js/jquery.mobile.custom.min.js') }}"></script> -->
	<script type="text/javascript" src="{{ url('assets/js/modernizr.js') }}"></script>
	<script type="text/javascript" src="{{ url('assets/js/app.js') }}"></script>
	<script type="text/javascript" src="{{ url('assets/js/main.js') }}"></script>

<!-- ------------------------------------------ app loaded js ------------------------------------------ -->
	@yield('scripts')

<!-- ------------------------------------------ template loaded js ------------------------------------------ -->
	<script type="text/javascript">
		@yield('inline-scripts')
	</script>

<!-- ------------------------------------------ google analytics js ------------------------------------------ -->
	<script type="text/javascript" src="{{ url('assets/js/analytics.js') }}"></script>
<!-- ------------------------------------------ google analytics js ------------------------------------------ -->

</body>
</html>
