@extends('_layouts.app')

@section('keywords')
keywords
@stop
@section('description')
description
@stop

{{-- Web site Title --}}
@section('title')
Careers @parent
@stop

@section('styles')
  <link rel="stylesheet" type="text/css" href="{{ url('careerapplication/contact.css') }}">
@stop

@section('scripts')
@stop

@section('inline-scripts')
$(document).ready(function() {

  $('.careerOptionCircle').hover(function() {
    $(this).addClass('animated pulse').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', 
      function() {
        $(this).removeClass('animated pulse');
      });
  });
    var frmvalidator  = new Validator("contactus");
    frmvalidator.EnableOnPageErrorDisplay();
    frmvalidator.EnableMsgsTogether();
    frmvalidator.addValidation("name","req","Please provide your name");

    frmvalidator.addValidation("email","req","Please provide your email address");

    frmvalidator.addValidation("email","email","Please provide a valid email address");

    frmvalidator.addValidation("message","maxlen=2048","The message is too long!(more than 2KB!)");

    frmvalidator.addValidation("photo","file_extn=jpg;jpeg;gif;png;bmp","Upload images only. Supported file types are: jpg,gif,png,bmp");

    frmvalidator.addValidation("scaptcha","req","Please enter the code in the image above");

    document.forms['contactus'].scaptcha.validator
      = new FG_CaptchaValidator(document.forms['contactus'].scaptcha,
                    document.images['scaptcha_img']);

    function SCaptcha_Validate()
    {
        return document.forms['contactus'].scaptcha.validator.validate();
    }

    frmvalidator.setAddnlValidationFunction("SCaptcha_Validate");

    function refresh_captcha_img()
    {
        var img = document.images['scaptcha_img'];
        img.src = img.src.substring(0,img.src.lastIndexOf("?")) + "?rand="+Math.random()*1000;
    }
});
@stop


{{-- Content --}}
@section('content')

<div style="margin-top: 95px;">
  <img src="{{ url('images/home/jax_team.jpg') }}" alt="Start your career with Bank of England Mortgage" class="img-responsive">
</div>
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="about-title">
        <h2>Bank of England Mortgage Jacksonville has been voted a Best Place to Work 2 years in a row!</h2>
        <p class="pLead">Bank of England Mortgage maintains all employment descriptions on its website at all times and does not indicate the position is open. Further descriptions and qualifications are available upon consideration of the position. Honesty and integrity in dealing with others and accountability in standing behind your work; initiative and self-motivation in serving the client, getting the job completed thoroughly and on time; ability to listen to others and understand their needs; confidence in yourself and enthusiasm regarding your service are required. If you are interested in applying for a position, please upload your resume below and indicate the position for which you are applying.</p>
      </div>
    </div>
  </div>
  <div class="row padding-top-lg">
    <h2 class="careerOptionHeading text-center padding-bottom-lg">Choose Your Bank of England Mortgage Jacksonville Branch</h2>
    <div class="col-md-6">
      <div class="careerOptionCircle">
        <div class="careerOptionCircle__text">
          <a href="/about/careers/branch_1112"> 13901 Sutton Park Drive S. Suite 403</a>
        </div>
      </div>
    </div>
    <div class="col-md-6">
      <div class="careerOptionCircle">
        <div class="careerOptionCircle__text">
          <a href="/about/careers/branch_1114"> 13901 Sutton Park Drive S. Suite 170</a>
        </div>
      </div>
    </div>
  </div>
</div>

@stop
