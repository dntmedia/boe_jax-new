@extends('_layouts.app')

@section('keywords')
keywords
@stop
@section('description')
description
@stop

{{-- Web site Title --}}
@section('title')
13901 Sutton Park Drive S Suite 170 Careers @parent
@stop

@section('styles')
@stop

@section('scripts')
@stop

@section('inline-scripts')
@stop


{{-- Content --}}
@section('content')

<div style="margin-top: 95px;">
  <img src="{{ url('images/home/jax_team.jpg') }}" alt="Start your career with Bank of England Mortgage" class="img-responsive">
</div>
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="about-title">
        <h1 class="text-center">
          13901 Sutton Park Drive S. Suite 170 
        </h1>
        <p class="lead text-center">The positions below are positions held at this branch. <br> To view and apply for open positions visit this <a href="https://workforcenow.adp.com/jobs/apply/posting.html?client=BOE1&ccId=19000101_000001&type=MP&lang=en_US">ADP</a>.</p>
        <div class="col-md-4 col-sm-6">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title"><span style="font-size:15px">Appraisal Coordinator/Administrative Assistant</span></h4>
            </div>
            <div class="panel-body">  
            </div>
          </div>
        </div>
        <div class="col-md-4 col-sm-6">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title">Credit Manager</h4>
            </div>
            <div class="panel-body">
            </div>
          </div>
        </div>
        <div class="col-md-4 col-sm-6">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title">Loan Officer Assistant</h4>
            </div>
            <div class="panel-body">
            </div>
          </div>
        </div>
        <div class="col-md-4 col-sm-6">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title">Mortgage Banker</h4>
            </div>
            <div class="panel-body">   
            </div>
          </div>
        </div>
        <div class="col-md-4 col-sm-6">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title">Loan Processor</h4>
            </div>
            <div class="panel-body">  
            </div>
          </div>
        </div>
        <div class="col-md-4 col-sm-6">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title">Underwriter</h4>
            </div>
            <div class="panel-body">  
            </div>
          </div>
        </div>
        <div class="col-md-4 col-sm-6">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title">Loan Closer</h4>
            </div>
            <div class="panel-body">  
            </div>
          </div>
        </div>
        <div class="col-md-4 col-sm-6">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title">Closing Disclosure Prep</h4>
            </div>
            <div class="panel-body">   
            </div>
          </div>
        </div>
        <div class="col-md-4 col-sm-6">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title">Marketing</h4>
            </div>
            <div class="panel-body">   
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


@stop
