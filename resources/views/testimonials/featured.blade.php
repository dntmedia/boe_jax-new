@extends('_layouts.app')

@section('keywords')
keywords
@stop
@section('description')
description
@stop


{{-- Web site Title --}}
@section('title')
Client Testimonials @parent
@stop

@section('styles')
@stop

@section('scripts')
@stop

@section('inline-scripts')
@stop


{{-- Content --}}
@section('content')



<section id="about" style="margin-top: 95px">
    <img src="{{ url('images/home/jax_team.jpg') }}" alt="Bank of England Mortgage Jacksonville" class="img-responsive">
    <div class="container testimonial-content">
        <div class="row">
          <h1>Top Testimonials</h1>
          @foreach($featured as $testimonial)

          <div class="testimonial-item">
            <blockquote>
              <p><em>
                {{ $testimonial->content }}
              </em></p>
              <footer class="text-right"><cite title="{{ $testimonial->person }}">{{ $testimonial->person }}</cite></footer>
            </blockquote>
          </div>

              <!-- <h1 class="text-center" style="margin-bottom: 40px">{{ $testimonial->person }}</h1>

              <p>
              {{ $testimonial->summary }}
              </p>

              <p>
              {{ $testimonial->content }}
              </p> -->

              <hr>

          @endforeach

        </div><!-- end row -->

    </div><!-- end container -->

</section>

@stop
