@extends('_layouts.app')

@section('keywords')
keywords
@stop
@section('description')
description
@stop


{{-- Web site Title --}}
@section('title')
Testimonials @parent
@stop

@section('styles')
@stop

@section('scripts')
@stop

@section('inline-scripts')
@stop


{{-- Content --}}
@section('content')

<br>
<br>
<br>

<section id="testimonial-banner">
  <div class="container">
    <div class="row">
      <div class="testimonial-content">
        <div class="col-lg-8 col-lg-offset-2">
          <div id="success-msg">
            <h1>  {{  $message }} </h1>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

@stop
