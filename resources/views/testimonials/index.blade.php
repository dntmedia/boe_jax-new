@extends('_layouts.app')

@section('keywords')
keywords
@stop
@section('description')
description
@stop


{{-- Web site Title --}}
@section('title')
Testimonials @parent
@stop

@section('styles')
@stop

@section('scripts')
@stop

@section('inline-scripts')
@stop


{{-- Content --}}
@section('content')
<div style="margin-top: 95px;"></div>
<div class="container testimonial-body" style="margin-bottom: 50px">
  <div class="row">
    <div class="flex-content">
      <div class="col-md-4 col-md-push-8">
        <h2>Client Testimonials</h2>
        <p class="lead">
          We take great pride in our work and nothing makes us happier to hear that our clients appreciate what we do. Please take a minute to look over some of our favorite testimonials and if you have your own BOE Mortgage Jacksonville story that you like to share, please do so! <em>Hovering over a letter will pause the rotation</em>.
        </p>
        <a href="/testimonials/create" class="btn main-btn">Submit a testimonial</a>
        <hr>
        <a href="/testimonials/all" class="btn main-btn">All Testimonials</a>
      </div>
      <div class="col-md-8 col-md-pull-4">
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel" data-interval="6000">
          <!-- Wrapper for slides -->
          <div class="carousel-inner" role="listbox">
            <div class="item active">
              <img src="{{ url('images/about/testimonials/Wolfson_Childrens_Hospital.jpg') }}" alt="Wolfson Childrens Hosptial" class="img-responsive">
            </div>
            <div class="item">
              <img src="{{ url('images/about/testimonials/TimNewman1.jpg') }}"  alt="Tim Newman Testimonial" class="img-responsive">
            </div>
            <div class="item">
              <img src="{{ url('images/about/testimonials/JannaThomas1.jpg') }}"  alt="Janna Thomas Testimonial" class="img-responsive">
            </div>
            <div class="item">
              <img src="{{ url('images/about/testimonials/Holmes.jpg') }}"  alt="Holmes Testimonial" class="img-responsive">
            </div>
            <div class="item">
              <img src="{{ url('images/about/testimonials/Davidson.jpg') }}"  alt="Davidson Realty Testimonial" class="img-responsive">
            </div>
            <div class="item">
              <img src="{{ url('images/about/testimonials/KellerWilliams.jpg') }}"  alt="Keller Williams Testimonial" class="img-responsive">
            </div>
            <div class="item">
              <img src="{{ url('images/about/testimonials/Exit2.jpg') }}"  alt="Exit Testimonial" class="img-responsive">
            </div>
            <div class="item">
              <img src="{{ url('images/about/testimonials/Exit.jpg') }}"  alt="Exit Testimonial 2" class="img-responsive">
            </div>
            <div class="item">
              <img src="{{ url('images/about/testimonials/Legends.jpg') }}"  alt="Legends Testimonial" class="img-responsive">
            </div>
            <div class="item">
              <img src="{{ url('images/about/testimonials/Tyler.jpg') }}"  alt="Tyler Testimonial" class="img-responsive">
            </div>
            <div class="item">
              <img src="{{ url('images/about/testimonials/WendyGriffs.jpg') }}"  alt="Wendy Griffs Testimonial" class="img-responsive">
            </div>
            <div class="item">
              <img src="{{ url('images/about/testimonials/Remax.jpg') }}"  alt="Remax Testimonial" class="img-responsive">
            </div>
            <div class="item">
              <img src="{{ url('images/about/testimonials/Davidson2.jpg') }}"  alt="Davidson Testimonial" class="img-responsive">
            </div>
            <div class="item">
              <img src="{{ url('images/about/testimonials/JessicaHallWestLoan.jpg') }}"  alt="Jessica Hall West Loan Testimonial" class="img-responsive">
            </div>
            <div class="item">
              <img src="{{ url('images/about/testimonials/KimSessionsBoothLoan.jpg') }}"  alt="Kim Sessions Booth Loan Testimonial" class="img-responsive">
            </div>
            <div class="item">
            <img src="{{ url('images/about/testimonials/McKennis_Allen_Testimony.jpg') }}" alt="McKennis Allen House Master  Home Inspection Testimonial" class="img-responsive">
            </div>
            <div class="item">
              <img src="{{ url('images/about/testimonials/George_Garcia_Testimonial.jpg') }}"  alt="First Beaches Insurance Testimonial for Rickie McWilliams" class="img-responsive">
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

@stop
