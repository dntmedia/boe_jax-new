@extends('_layouts.app')

@section('keywords')
keywords
@stop
@section('description')
description
@stop


{{-- Web site Title --}}
@section('title')
Testimonials @parent
@stop

@section('styles')
@stop

@section('scripts')
@stop

@section('inline-scripts')
@stop


{{-- Content --}}
@section('content')
<div class="section-top"></div>
<div class="container testimonial-body">
    <div class="row">
        @if (count($errors) > 0)
        <!-- Form Error List -->
        <div class="alert alert-danger">
            <strong>Whoops!</strong> Something went wrong!.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        <form class="form-horizontal" role="form" method="POST" action="{{ route('testimonials.store') }}">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Please leave a testimonial
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Your Name</label>
                        <div class="col-sm-10">
                            <input id="form_name" type="text" name="name" class="form-control" placeholder="Please enter your name." required="required" data-error="Your name is required.">
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                    <div class="col-md-12 contact_hidden">
                        <div class="form-group">
                            <label for="form_email">email</label>
                            <input id="form_email" type="email" name="email" class="form-control" placeholder="Please enter your email address">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Loan Officer</label>
                        <div class="col-sm-10">
                            <select id="loan_officer" name="loan_officer">
                                <option value='0' selected>-- Select a Loan Officer --</option>
                                @foreach ($officers as $officer)
                                    <option value='{{ $officer['id'] }}'>{{ $officer['fname'] . ' ' . $officer['lname'] }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Summary</label>
                        <div class="col-sm-10">
                            <textarea id="form_message" name="summary" class="form-control" placeholder="A summary is not required but it would be nice to use your words." rows="3"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Testimonial</label>
                        <div class="col-sm-10">
                            <textarea id="form_message" name="message" class="form-control" placeholder="Please write your testimonial here." rows="10" required="required" data-error="Please,leave us a message."></textarea>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <button type="submit" class="btn btn-sm main-btn"><i class="glyphicon glyphicon-ok"></i>Create</button>
                    <a href="{{ route('testimonials.index') }}" class="btn btn-default btn-sm main-btn btn-addon"><i class="glyphicon glyphicon-remove"></i>Cancel</a>
                </div>
            </div>
        </form>
    </div>
</div>


@stop
