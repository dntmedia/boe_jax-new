@extends('_layouts.app')

@section('keywords')
keywords
@stop
@section('description')
description
@stop


{{-- Web site Title --}}
@section('title')
Client Testimonials @parent
@stop

@section('styles')
@stop

@section('scripts')
@stop

@section('inline-scripts')
@stop


{{-- Content --}}
@section('content')

<section id="about" style="margin-top: 95px">
  <img src="{{ url('images/home/jax_team.jpg') }}" alt="Bank of England Mortgage Jacksonville" class="img-responsive">
  <div class="container testimonial-content">
    <div class="row">
      <h1>Client Testimonials</h1>
        @foreach($all as $testimonial)
        <div class="testimonial-item">
          <blockquote>
            <p><em>
              {{ $testimonial->content }}
            </em></p>
            <footer class="text-right"><cite title="{{ $testimonial->person }}">{{ $testimonial->person }}</cite></footer>
          </blockquote>
        </div>

        <hr>

        @endforeach

    </div><!-- end row -->
  </div><!-- end container -->
</section>

@stop
