@extends('_layouts.app')

@section('keywords')
keywords
@stop
@section('description')
description
@stop


{{-- Web site Title --}}
@section('title')
Analytics @parent
@stop

@section('styles')
@stop

@section('scripts')
@stop

@section('inline-scripts')
@stop


{{-- Content --}}
@section('content')

<section class="margin-top-b">
  <div class="container">
    <h2>Website Analytics Policy</h2>
    <p class="lead">BOE Mortgage was founded on principles of trust, integrity and the delivery of outstanding service. Therefore, we are very concerned with the privacy rights of our online visitors and customers and are committed to protecting the information collected about you. This website, BOEJax.com, is owned and operated by Bank of England Mortgage, has taken extensive measures to protect the confidentiality of your personal information and to protect your data from misuse and unauthorized access or disclosure. Unfortunately, no data transmission over the Internet can be guaranteed to be 100% secure. As a result, although we take great measures to protect your information, BOE Mortgage cannot ensure or warrant the security of any information you transmit to us, and you do so at your own risk. Once we receive your transmission, BOE Mortgage takes industry standard efforts to safeguard the confidentiality of your information, such as secure, 128 bit encryption on our applications. We also restrict internal access of stored data to select employees that have been informed of our privacy policies. If you have concerns involving the security or privacy of your information, you can arrange to talk to one of our loan specialists.</p>

    <h3>Information Collected at our Web site</h3>

    <h4>Personal Information</h4>
    <p>If you submit a mortgage application to BOE Mortgage you are required to provide certain information necessary to deliver the selected services. In some instances you will only provide personal information such as name, address, e-mail address, phone number and other contact information. The more information you share with BOE Mortgage the more accurately we will be able to provide you with information and services you have indicated are of interest.</p>

    <h4>Statistical Information</h4>
    <p>We automatically collect anonymous statistical information regarding usage of our Web site. This helps us understand our Customers' needs better and improve areas of our services that our Customers find valuable. For instance, when you visit www.BOEJax.com, our Web site recognizes the type of browser you are using (Firefox, Safari, IE, etc.), the site that linked you to our site, the pages you visit at our site, and similar information. None of this information identifies you personally.</p>

    <h4>Cookies</h4>
    <p>Although we believe the use cookies is useful in tracking site usage trends and patterns, as while implementing individual user preferences, BOEJax.com does not use cookies at this time. BOE Mortgage does use cookies related our search term marketing efforts but this does not track any information on you as an individual.</p>

    <h4>Use and Disclosure of Information</h4>
    <p>With your prior consent, we may disclose any information we collect, as described above, to BOE Mortgage affiliates or service providers to perform a service for us or perform a function on our behalf. In cases where we use service providers, we restrict such service providers from using your non-public personal information except to act on our behalf or as required or permitted by law.</p>

    <h4>Removal of Personal Information</h4>
    <p>You can request that we purge the entirety of the personal information submitted by you through BOEJax.com from our database by providing detailed instructions to BOE Mortgage's Privacy Officer (see below). In such cases BOE Mortgage will delete all digitally warehoused information you have submitted. Please note that BOE Mortgage cannot purge information from the systems or files of our BOE Mortgage affiliates, service providers or other financial institutions that have subsequently been sent your personal information to provide their services.</p>

    <h4>Terms of Use</h4>
    <p>BOE Mortgage provides the BOEJax.com Internet site "as is" to the user without any warranty or condition, express, implied or statutory. BOE Mortgage disclaims any implied warranties of title, merchantability, non-infringement of intellectual property or fitness for any particular purpose.</p>
    <p>Except as may be otherwise expressly provided by written agreement between BOE Mortgage and the user, BOE Mortgage will have no tort, contract or any other liability to the user and/or any third party arising in connection with the use of this Internet site, or reliance on any information or services provided at this Internet site.</p>
    <p>In the event of errors or omissions in any information BOE Mortgage's sole responsibility shall be to use its best efforts to correct, by the issue of a corrected release, any errors or omissions which it or any of its employees, servants or agents may have made, negligently or otherwise, in transmitting copy accurately and which are brought to its attention. Without limiting the generality of the foregoing, in no event shall BOE Mortgage or any of its servants, employees or agents be liable in contract, tort or otherwise to any person (including, without limitation, the subscriber) for any loss, damage, injury, liability, cost or expense of any nature (including, without limitation, direct, indirect, consequential, incidental, special and punitive damages) which such person may suffer or incur as a result of any such errors or omissions or any delays in transmission or non-transmission.</p>

    <h4>Your Acceptance Of These Terms</h4>
    <p>By using this site, or by submitting personal information to us, you signify your agreement to BOE Mortgage's Privacy Policy and Terms Of Use. From time to time we may change or update our privacy policies. It is your responsibility to check back regularly to inform yourself of such changes. Your continued use of the BOEJax.com site following the posting of changes to these terms will be deemed as your acceptance of those changes. The foregoing policies are in effect as of June 1, 2005. BOE Mortgage reserves the right to change this policy statement at any time by posting the revised policy on this Web site. This statement and the policies outlined herein are not intended to and do not create any contractual or other legal rights in or on behalf of any party.</p>

    <p>If you have any further questions or concerns, please contact:</p>

    <address>
      Bank of England Mortgage <br />
      13901 Sutton Park Drive S. Suite 170 & 403 <br />
      Jacksonville, FL 32224 <br />
    </address>
  </div>
</section>

@stop
