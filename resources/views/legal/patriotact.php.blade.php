@extends('_layouts.app')

@section('keywords')
keywords
@stop
@section('description')
description
@stop


{{-- Web site Title --}}
@section('title')
@parent :: United States Patriot Act
@stop

@section('styles')
@stop

@section('scripts')
@stop

@section('inline-scripts')
@stop


{{-- Content --}}
@section('content')
<section class="margin-top-b">
  <div class="container">
    <h2>United States Patriot Act</h2>
    <p class="text-uppercase">IMPORTANT INFORMATION ABOUT PROCEDURES FOR OPENING OR CHANGING AN ACCOUNT WITH BANK OF ENGLAND MORTGAGE</p>

    <p>Section 326 of the USA PATRIOT ACT requires all financial institutions to obtain, verify, and record information that identifies each person who opens an account or changes an existing account. This federal requirement applies to all new customers and current customers. This information is used to assist the United States government in the fight against the funding of terrorism and money-laundering activities.</p>

    <p>What this means to you: when you open an account or change an existing account, we will ask each person for their name, physical address, mailing address, date of birth, and other information that will allow us to identify them. We will ask to see each person’s driver’s license and other identifying documents and copy or record information from each of them.</p>
  </div>
</section>

@stop
