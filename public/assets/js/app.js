$(document).ready(function() {

	$('#quote-carousel').carousel({
    pause: true, interval: 10000,
  	});

  	$('[data-toggle="popover"]').popover();

  	$('.dropdown-submenu a.test').on("click", function(e){
    $(this).next('ul').toggle();
    e.stopPropagation();
    e.preventDefault();
  });

});
