<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Office extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'offices';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        // 'name',
        // 'email',
        // 'password'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        // 'password',
        // 'remember_token'
    ];

    // public static function findState($state)
    // {

    //     dd($state);

    //     global $conn;
    //     $result = array();

    //     # create query
    //     $sql1 = "select * from `offices` where `STATE` = :state and ACTIVE !='No' order by NAME";
    //     $rs = $conn->prepare($sql1);
    //     $rs->execute(array('state' => $state));

    //     # write the data
    //     while ($row = $rs->fetch()) {
    //         $office_name = $row["NAME"];
    //         $office_address = $row["ADDRESS"];
    //         $office_city = $row["CITY"];
    //         $office_state = $row["STATE"];
    //         $office_zip = $row["ZIP"];
    //         $office_phone = $row["PHONE"];
    //         $office_fax = $row['FAX'];
    //         $office_manager = $row['MANAGER'];
    //         $office_id = $row['ID'];
    //         $office_url = $row['URL'];

    //         $result[$office_name] = array("name" => $office_name, "address" => $office_address, "city" => $office_city, "state" => $office_state, "zip" => $office_zip, "phone" => $office_phone, "fax" => $office_fax, "manager" => $office_manager, "id" => $office_id, "url" => $office_url);
    //     }

    //     return $result;
    // }

}
