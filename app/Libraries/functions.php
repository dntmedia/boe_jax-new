<?php

namespace App\Libraries;

function findClosest($zip, $radius)
{
    global $conn;
    $result = array();

    # create query
    $sql1 = "select * from `zipcodes` where `ZIP` = :zip";

    # execute query
    $rs = $conn->prepare($sql1);
    $rs->execute(array('zip' => $zip));

    # write the data
    while ($row = $rs->fetch()) {
        $lon1 = $row["LONGITUDE"];
        $lat1 = $row["LATITUDE"];
        $city1 = $row["CITY"];
        $state1 = $row["STATE"];
    }

# create query

    $sql2 = "select * from offices WHERE ACTIVE != 'No'";

    # execute query
    $rs2 = $conn->query($sql2);

    # write the data
    while ($row = $rs2->fetch()) {
        $office_name = $row["NAME"];
        $office_address = $row["ADDRESS"];
        $office_city = $row["CITY"];
        $office_state = $row["STATE"];
        $office_zip = $row["ZIP"];
        $office_phone = $row["PHONE"];
        $office_fax = $row['FAX'];
        $office_manager = $row['MANAGER'];
        $office_id = $row['ID'];
        $office_url = $row['URL'];

        # TAKE THE DEALER ZIPCODE FROM ABOVE AND RUN THE LONG & LATS THROUGH THE FUNCTION
        # create query
        $office_lookup = "select * from `zipcodes` where `ZIP` = :dzip";

        # execute query
        $office_rs = $conn->prepare($office_lookup);
        $office_rs->execute(array('dzip' => $office_zip));

        # write the data
        while ($row = $office_rs->fetch()) {
            $lon2 = $row["LONGITUDE"];
            $lat2 = $row["LATITUDE"];

            $distance = (3958 * 3.1415926 * sqrt(($lat2 - $lat1) * ($lat2 - $lat1) + cos($lat2 / 57.29578) * cos($lat1 / 57.29578) * ($lon2 - $lon1) * ($lon2 - $lon1)) / 180);

            $round_distance = round($distance, 1);

            if ($round_distance < $radius) {
                $result[$office_name] = array("name" => $office_name, "address" => $office_address, "city" => $office_city, "state" => $office_state, "zip" => $office_zip, "phone" => $office_phone, "fax" => $office_fax, "manager" => $office_manager, "id" => $office_id, "url" => $office_url);
            }
        }
    }
    return $result;
}

function findState($state)
{
    global $conn;
    $result = array();

    # create query
    $sql1 = "select * from `offices` where `STATE` = :state and ACTIVE !='No' order by NAME";
    $rs = $conn->prepare($sql1);
    $rs->execute(array('state' => $state));

    # write the data
    while ($row = $rs->fetch()) {
        $office_name = $row["NAME"];
        $office_address = $row["ADDRESS"];
        $office_city = $row["CITY"];
        $office_state = $row["STATE"];
        $office_zip = $row["ZIP"];
        $office_phone = $row["PHONE"];
        $office_fax = $row['FAX'];
        $office_manager = $row['MANAGER'];
        $office_id = $row['ID'];
        $office_url = $row['URL'];

        $result[$office_name] = array("name" => $office_name, "address" => $office_address, "city" => $office_city, "state" => $office_state, "zip" => $office_zip, "phone" => $office_phone, "fax" => $office_fax, "manager" => $office_manager, "id" => $office_id, "url" => $office_url);
    }

    return $result;
}

function chooseProduct($user_a)
{

    global $conn;
    $i = 1;

    $results = explode(',', $user_a);
    foreach ($results as $key => $value) {

        $query = "select `PRODUCT` from `concierge_a` where `QID` = '$i' and `LETTER` = :abc";
        $select = $conn->prepare($query);
        $select->execute(array('abc' => $value));

        while ($sets = $select->fetch()) {
            $product = $sets['PRODUCT'];
            $test .= $product . ",";
            $list[] = $product;

        }

        $i++;
    }

    $count = array_count_values($list);
    arsort($count);

    $result = key($count);

    return $result;

}

function getOffices()
{
    global $conn;
    $result = array();

    $query = "select * from `offices`";
    $select = $conn->prepare($query);
    $select->execute();
    $result = $select->fetchAll();

    return $result ? $result : array();
}

function getOffice($id)
{
    global $conn;

    $query = "select * from `offices` where `ID` = :id";
    $select = $conn->prepare($query);
    $select->execute(array('id' => $id));
    $result = $select->fetch();

    return $result;
}

function updateOffice($id, $data)
{
    global $conn;

    $query = "update `offices` set `BRANCH` = :branch,
		`NAME` = :name,
		`ADDRESS` = :address,
		`CITY` = :city,
		`STATE` = :state,
		`ZIP` = :zip,
		`MANAGER` = :manager,
		`EMAIL` = :email,
		`PHONE` = :phone,
		`FAX` = :fax,
		`ACTIVE` = :active,
		`URL` = :url
		where `ID` = :id";
    $update = $conn->prepare($query);
    $status = $update->execute(array(
        'branch' => $data['BRANCH'],
        'name' => $data['NAME'],
        'address' => $data['ADDRESS'],
        'city' => $data['CITY'],
        'state' => $data['STATE'],
        'zip' => $data['ZIP'],
        'manager' => $data['MANAGER'],
        'email' => $data['EMAIL'],
        'phone' => $data['PHONE'],
        'fax' => $data['FAX'],
        'active' => isset($data['ACTIVE']) ? 'Yes' : 'No',
        'url' => $data['URL'],
        'id' => $id,
    ));

    return $status;
}

function insertOffice($data)
{
    global $conn;

    $query = "insert into `offices` values (
		NULL,
		:branch,
		:name,
		:address,
		:city,
		:state,
		:zip,
		:manager,
		:email,
		:phone,
		:fax,
		:active,
		:url
	)";
    $update = $conn->prepare($query);
    $status = $update->execute(array(
        'branch' => $data['BRANCH'],
        'name' => $data['NAME'],
        'address' => $data['ADDRESS'],
        'city' => $data['CITY'],
        'state' => $data['STATE'],
        'zip' => $data['ZIP'],
        'manager' => $data['MANAGER'],
        'email' => $data['EMAIL'],
        'phone' => $data['PHONE'],
        'fax' => $data['FAX'],
        'active' => isset($data['ACTIVE']) ? 'Yes' : 'No',
        'url' => $data['URL'],
    ));

    return $conn->LastInsertId();
}

function deleteOffice($id)
{
    global $conn;

    $query = "delete from `offices` where `id` = :id";
    $delete = $conn->prepare($query);
    $status = $delete->execute(array('id' => $id));

    return $status;
}
