<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
 */

// $app->get('/', function () use ($app) {
//     return $app->version();
// });

// Basics ---------------------------------------------------------------------

$app->get('/', 'PageController@home');

// pages  ---------------------------------------------------------------------

$app->get('calendar', 'PageController@calendar');
$app->get('community', 'PageController@community');
$app->get('videos', 'PageController@videos');
$app->get('apply', 'PageController@apply');
$app->get('loan-process', 'PageController@loanProcess');

// -- contact -----------------------------------------------------------------

$app->get('contact', 'ContactController@contact');
$app->post('send-contact', 'ContactController@postContact');

// careers  ---------------------------------------------------------------------

$app->get('about/careers', 'CareerController@careers');
$app->get('about/careers/branch_1112', 'CareerController@branch_1112');
$app->get('about/careers/branch_1114', 'CareerController@branch_1114');

// about  ---------------------------------------------------------------------

$app->get('about/partners', 'AboutController@partners');
$app->get('about/recognition', 'AboutController@recognition');
$app->get('about/who-we-are', 'AboutController@whoWeAre');

// branch  ---------------------------------------------------------------------

$app->get('about/branches', 'BranchController@branches');
$app->get('about/branch/{id}', 'BranchController@getBranchTeam');
$app->get('about/our-team', 'BranchController@ourTeam');
$app->get('about/our-team/{id}', 'BranchController@getOfficer');

// legal  ---------------------------------------------------------------------

$app->get('legal/analytics', 'LegalController@analytics');
$app->get('legal/counseling', 'LegalController@counseling');
$app->get('legal/identity_theft', 'LegalController@identitytheft');
$app->get('legal/patriot_act', 'LegalController@patriotact');
$app->get('legal/security', 'LegalController@security');

// products  ------------------------------------------------------------------

$app->get('products/bridge-loans', 'ProductsController@bridgeLoans');
$app->get('products/bond-loans', 'ProductsController@bondLoans');
$app->get('products/fha-loans', 'ProductsController@fhaLoans');
$app->get('products/jumbo-loans', 'ProductsController@jumboLoans');
$app->get('products/renovation', 'ProductsController@renovation');
$app->get('products/reverse-mortgage', 'ProductsController@reverseMortgage');
$app->get('products/usda-loans', 'ProductsController@usdaLoans');
$app->get('products/va-loans', 'ProductsController@vaLoans');
$app->get('products/conventional', 'ProductsController@conventional');
$app->get('products/vacation', 'ProductsController@vacation');

// testimonials  ---------------------------------------------------------------------

$app->get('testimonials/all', 'TestimonialsController@all');
$app->get('testimonials/create', 'TestimonialsController@create');
$app->get('testimonials/featured', 'TestimonialsController@featured');

$app->get('testimonials', [
    'as' => 'testimonials.index', 'uses' => 'TestimonialsController@index',
]);
$app->post('testimonials', [
    'as' => 'testimonials.store', 'uses' => 'TestimonialsController@store',
]);

// tools  ---------------------------------------------------------------------

$app->get('tools/fastapp', 'ToolsController@fastapp');
$app->get('tools/glossary', 'ToolsController@glossary');
$app->get('tools/mortgage-calculator', 'ToolsController@mortgageCalculator');
$app->get('tools/affordability', 'ToolsController@affordability');
$app->get('tools/rent-vs-buy', 'ToolsController@rentBuy');

$app->get('api/calculate-mortgage', 'ToolsController@calculateMortgage');
