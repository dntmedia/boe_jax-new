<?php

namespace App\Http\Controllers;

use DB;

class BranchController extends Controller
{
    /*
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

// View -----------------------------------------------------------------------

    /*
     * View : recognition
     *
     * @return view
     */
    public function branches()
    {
        return view('branch.branches');
    }

    /*
     * View : our-team
     *
     * @return view
     */
    public function getBranchTeam($id)
    {
        $pathOrUrl = "https://www.boefastapp.net/V2/index.php?token=" . env('API_TOKEN') . "&action=show_team&branch=" . $id;
        $json = file_get_contents($pathOrUrl);
        $officers = json_decode($json, true);

        if ($id == 1114) {
            $branch_address = env('BRANCH_ADDRESS_1114');
        } else {
            $branch_address = env('BRANCH_ADDRESS_1112');
        }

        // foreach ($officers as $officer) {

        //     if ($officer['team'] != "") {
        //         $memebers = explode(',', $officer['team']);
        //         // $count = count($memebers);

        //         // print_r($memebers);
        //         // echo "<br>";
        //         // echo $count;
        //         // echo "<br>";

        //         foreach ($memebers as $key => $value) {
        //             $pathOrUrl = "https://www.boefastapp.net/V2/index.php?token=" . env('API_TOKEN') . "&action=show_member&id=" . $value;
        //             $json = file_get_contents($pathOrUrl);
        //             $content = json_decode($json, true);
        //             echo $content[0]['fname'];
        //             echo "<br>";
        //         }

        //     }

        // }

        // dd("die");
        // dd($officers);

        return view('branch.branch_team',
            compact(
                'branch_address',
                'id',
                'officers'
            ));

    }

    /*
     * View : getOfficer
     *
     * @return view
     */
    public function getOfficer($id)
    {
        $pathOrUrl = "https://www.boefastapp.net/V2/index.php?token=" . env('API_TOKEN') . "&action=show_member&id=" . $id;
        $json = file_get_contents($pathOrUrl);
        $content = json_decode($json, true);

        $first_name = $content[0]['fname'];
        $last_name = $content[0]['lname'];
        $job_title = $content[0]['title'];
        $nmls = $content[0]['nmls'];
        $office = $content[0]['phone'];
        $mobile = $content[0]['mobile'];
        $fax = $content[0]['fax'];
        $email = $content[0]['email'];
        $apply_link = '/apply';
        $biography = $content[0]['biography'];
        $photo = $content[0]['photo'];

        $employee_info = DB::select('SELECT * FROM employee_extra where employee_id=:id and active=:active', ['id' => $id, 'active' => 1]);
        $testimonials = DB::select('SELECT * FROM testimonials where employee_id=:id and approved=:approved', ['id' => $id, 'approved' => 'Yes']);

        // dd($employee_info);

        return view('branch.officer',
            compact(
                'first_name',
                'last_name',
                'job_title',
                'nmls',
                'office',
                'mobile',
                'fax',
                'email',
                'apply_link',
                'photo',
                'biography',
                'employee_info',
                'testimonials'
            ));
    }

    /*
     * View : our-team
     *
     * @return view
     */
    public function ourTeam()
    {
        $pathOrUrl = "https://www.boefastapp.net/V2/index.php?token=" . env('API_TOKEN') . "&action=show_team&branch=" . env('BRANCH_ID');
        $json = file_get_contents($pathOrUrl);
        $officers = json_decode($json, true);

        if (env('BRANCH_ID_2') != "") {
            $pathOrUrl2 = "https://www.boefastapp.net/V2/index.php?token=" . env('API_TOKEN') . "&action=show_team&branch=" . env('BRANCH_ID_2');
            $json2 = file_get_contents($pathOrUrl2);
            $officers2 = json_decode($json2, true);
        } else {
            $officers2 = null;
        }

        return view('branch.our-team',
            compact(
                'officers',
                'officers2'
            ));

    }

}
