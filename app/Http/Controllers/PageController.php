<?php

namespace App\Http\Controllers;

use DB;

class PageController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(
//        Office $office
    ) {
//        $this->office = $office;
    }

    /**
     * View : welcome
     *
     * @return view
     */
    // public function welcome()
    // {
    //     return view('pages.welcome');
    // }

// Basics ---------------------------------------------------------------------

    /**
     * View : home
     *
     * @return view
     */
    public function home()
    {

        $branch_id = env('BRANCH_ID');
        $branch_id_2 = env('BRANCH_ID_2');

        $featured = DB::table('testimonials')
            ->whereBetween('rank', [1, 10])
            ->where('branch_id', '=', $branch_id)
            ->orWhere('branch_id', '=', $branch_id_2)
            ->where('approved', '=', 'Yes', 'AND')
            ->orderBy('rank', 'desc')
            ->get();

        //dd(count($featured));

        return view('pages.home',
            compact(
                'featured'
            ));
    }

    /**
     * View : about
     *
     * @return view
     */
    // public function about()
    // {
    //     return view('pages.about');
    // }

    /**
     * View : terms and conditions
     *
     * @return view
     */
    // public function terms()
    // {
    //     return view('pages.terms');
    // }

    /**
     * View : privacy policy
     *
     * @return view
     */
    // public function privacy()
    // {
    //     return view('pages.privacy');
    // }

// Site Specific --------------------------------------------------------------

    /**
     * View : blog
     *
     * @return view
     */
    public function blog()
    {
        return view('pages.blog');
    }

    /**
     * View : calendar
     *
     * @return view
     */
    public function calendar()
    {
        return view('pages.calendar');
    }

    /**
     * View : community
     *
     * @return view
     */
    public function community()
    {
        return view('pages.community');
    }

    /**
     * View : loan-process
     *
     * @return view
     */
    public function loanProcess()
    {
        return view('pages.loan-process');
    }

    /**
     * View : fastapp
     *
     * @return view
     */
    public function fastapp()
    {
        return view('pages.fastapp');
    }

    /**
     * View : videos
     *
     * @return view
     */
    public function videos()
    {
        return view('pages.videos');
    }
    /**
     * View : apply
     *
     * @return view
     */
    public function apply()
    {
        $extended_footer = false;
        $header_contact = true;
        return view('pages.apply',
            compact(
                'extended_footer',
                'header_contact'
            ));
    }

}
