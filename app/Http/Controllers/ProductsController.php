<?php

namespace App\Http\Controllers;

class ProductsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

// View -----------------------------------------------------------------------

    /**
     * View : bridge-loans
     *
     * @return view
     */
    public function bridgeLoans()
    {
        return view('products.bridge-loans');
    }

    /**
     * View : bond-loans
     *
     * @return view
     */
    public function bondLoans()
    {
        return view('products.bond-loans');
    }

    /**
     * View : fha-loans
     *
     * @return view
     */
    public function fhaLoans()
    {
        return view('products.fha-loans');
    }

    /**
     * View : home-equity
     *
     * @return view
     */
    public function homeEquity()
    {
        return view('products.home-equity');
    }

    /**
     * View : jumbo-loans
     *
     * @return view
     */
    public function jumboLoans()
    {
        return view('products.jumbo-loans');
    }

    /**
     * View : renovation
     *
     * @return view
     */
    public function renovation()
    {
        return view('products.renovation');
    }

    /**
     * View : reverse-mortgage
     *
     * @return view
     */
    public function reverseMortgage()
    {
        return view('products.reverse-mortgage');
    }

    /**
     * View : usda-loans
     *
     * @return view
     */
    public function usdaLoans()
    {
        return view('products.usda-loans');
    }

    /**
     * View : conventional
     *
     * @return view
     */
    public function conventional()
    {
        return view('products.conventional');
    }
    /**
     * View : va-loans
     *
     * @return view
     */
    public function vaLoans()
    {
        return view('products.va-loans');
    }

    /**
     * View : vacation
     *
     * @return view
     */
    public function vacation()
    {
        return view('products.vacation');
    }

}
