<?php

namespace App\Http\Controllers;

class CareerController extends Controller
{
    /*
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

// View -----------------------------------------------------------------------

    /*
     * View : careers
     *
     * @return view
     */
    public function careers()
    {
        $extended_footer = false;
        $header_contact = true;
        return view('careers.careers',
            compact(
                'extended_footer',
                'header_contact'
            ));
    }
        /*
     * View : branch_1112
     *
     * @return view
     */
    public function branch_1112()
    {
        $extended_footer = false;
        $header_contact = true;
        return view('careers.branch_1112',
            compact(
                'extended_footer',
                'header_contact'
            ));
    }
        /*
     * View : branch_1114
     *
     * @return view
     */
    public function branch_1114()
    {
        $extended_footer = false;
        $header_contact = true;
        return view('careers.branch_1114',
            compact(
                'extended_footer',
                'header_contact'
            ));
    }



}
