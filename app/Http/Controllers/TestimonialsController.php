<?php

namespace App\Http\Controllers;

use App\Models\Testimonial;
use DB;
use Illuminate\Http\Request;
use Validator;

class TestimonialsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Testimonial $testimonial)
    {
        $this->testimonial = $testimonial;
    }

// View -----------------------------------------------------------------------

    /*
     * View : index
     *
     * @return view
     */
    public function index()
    {
        $picRandom = rand(1, 7);

        return view('testimonials.index',
            compact(
                'picRandom'
            ));
    }

    /*
     * View : create
     *
     * @return view
     */
    public function create()
    {

        $errors = null;

        $branch_id = env('BRANCH_ID');
        $branch_id_2 = env('BRANCH_ID_2');

        $pathOrUrl = "https://www.boefastapp.net/V2/index.php?token=" . env('API_TOKEN') . "&action=show_team&branch=" . $branch_id;
        $json_1 = file_get_contents($pathOrUrl);
        // $officers_1 = json_decode($json, true);

        $pathOrUrl = "https://www.boefastapp.net/V2/index.php?token=" . env('API_TOKEN') . "&action=show_team&branch=" . $branch_id_2;
        $json_2 = file_get_contents($pathOrUrl);
        // $officers_2 = json_decode($json, true);

        $officers = json_encode(array_merge(json_decode($json_1, true), json_decode($json_2, true)));
        $officers = json_decode($officers, true);

        // dd(count($officers));

        return view('testimonials.create',
            compact(
                'errors',
                'officers'
            ));

    }

    /*
     * View : N/A database storage function
     *
     * @return view
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
        ]);

        // if ($validator->fails()) {
        //     return redirect()->back()->withErrors($validator)->withInput();
        // }
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors());
        }

        $branch = DB::table('employee_extra')
            ->where('employee_id', '=', $request->input('loan_officer'))
            ->first();

        // dd($branch->branch_id);

        $testimonial = new $this->testimonial;

        $testimonial->person = $request->input('name');
        $testimonial->employee_id = $request->input('loan_officer');
        $testimonial->summary = $request->input('summary');
        $testimonial->content = $request->input('message');

        $testimonial->approved = 'No';
        $testimonial->branch_id = $branch->branch_id;

        $testimonial->save();

        // return redirect()->route('testimonial.index');
        $from = env('TESTIMONIAL_FROM', '');
        $sendTo = env('TESTIMONIAL_TO', '');
        $subject = env('TESTIMONIAL_SUBJECT', 'New Testimonial from testimonial form');

        $fields = array('name' => 'Name', 'surname' => 'Surname', 'phone' => 'Phone', 'email' => 'Email', 'message' => 'Message'); // array variable name => Text to appear in email
        $okMessage = 'Your testimonial was successfully submitted. Thank you!';
        $errorMessage = 'There was an error while submitting the form. Please try again';

// let's do the sending

        if (empty($_POST['email'])) {
            try
            {
                $emailText = "You have a new testimonial\n=============================\n";

                // foreach ($_POST as $key => $value) {

                //     if (isset($fields[$key])) {
                //         $emailText .= "$fields[$key]: $value\n";
                //     }
                // }

                mail($sendTo, $subject, $emailText, "From: " . $from);

                $responseArray = array('type' => 'success', 'message' => $okMessage);
            } catch (\Exception $e) {
                $responseArray = array('type' => 'danger', 'message' => $errorMessage);
            }

            // if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            //     $encoded = json_encode($responseArray);

            //     header('Content-Type: application/json');

            //     echo $encoded;
            // } else {
            //     echo $responseArray['message'];
            // }

            $message = $responseArray['message'];

        } // honeypot -- check for address being filled or not

        // return redirect()->route($uri);
        return view('testimonials.success',
            compact(
                'message'
            ));

    }

    /*
     * View : all
     *
     * @return view
     */
    public function all()
    {

        $branch_id = env('BRANCH_ID');
        $branch_id_2 = env('BRANCH_ID_2');

        $all = DB::table('testimonials')
            ->where('branch_id', '=', $branch_id)
            ->orWhere('branch_id', '=', $branch_id_2)
            ->where('approved', '=', 'Yes', 'AND')
            ->get();

        // dd($id);

        return view('testimonials.all',
            compact(
                'all'
            ));
    }

    /*
     * View : featured
     *
     * @return view
     */
    public function featured()
    {

        $branch_id = env('BRANCH_ID');
        $branch_id_2 = env('BRANCH_ID_2');

        $featured = DB::table('testimonials')
            ->whereBetween('rank', [1, 10])
            ->where('branch_id', '=', $branch_id)
            ->orWhere('branch_id', '=', $branch_id_2)
            ->where('approved', '=', 'Yes', 'AND')
            ->orderBy('rank', 'desc')
            ->get();

        // dd($id);

        return view('testimonials.featured',
            compact(
                'featured',
                'id'
            ));
    }

}
