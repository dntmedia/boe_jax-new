<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;

class LocaterController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * View : find-branch
     *
     * @return view
     */
    public function findBranch()
    {

        $results = "No";
        $searched = null;
        $test = null;

        return view('pages.find-branch',
            compact(
                'results',
                'searched',
                'test'
            ));
    }

    /**
     * View : find-branch
     *
     * @return view
     */
    public function postBranch($state)
    {

        $results = "Yes";
        $searched = "All offices within $state";

        $offices = DB::table('offices')
            ->where('STATE', '=', $state)
            ->where('ACTIVE', '!=', 'NO', 'AND')
            ->orderBY('name')
            ->get();
        // dd($offices);

        return view('pages.find-branch',
            compact(
                'offices',
                'results',
                'searched'
            ));
    }

    /**
     * View : find-branch
     *
     * @return view
     */
    public function postBranchZip(
        Request $request
    ) {

        $radius = $request->radius;
        $zip = $request->zip;

        $results = "LOOKUP";
        $searched = "All offices within $radius miles of $zip";

        $zipcode = DB::table('zipcodes')
            ->where('ZIP', '=', $zip)
            ->first();

//        dd($zipcode);

        $lon1 = $zipcode->LONGITUDE;
        $lat1 = $zipcode->LATITUDE;
        $city1 = $zipcode->CITY;
        $state1 = $zipcode->STATE;

        $office_lookups = [];
# create query

        $offices = DB::table('offices')
            ->where('ACTIVE', '!=', 'NO')
            ->get();

        foreach ($offices as $office) {
            $office_name = $office->NAME;
            $office_branch = $office->BRANCH;
            $office_address = $office->ADDRESS;
            $office_city = $office->CITY;
            $office_state = $office->STATE;
            $office_zip = $office->ZIP;
            $office_phone = $office->PHONE;
            $office_fax = $office->FAX;
            $office_manager = $office->MANAGER;
            $office_id = $office->ID;
            $office_url = $office->URL;
            $office_app_name = $office->APP_NAME;
            $office_app_url = $office->APP_URL;

            $office_lookup = DB::table('zipcodes')
                ->where('ZIP', '=', $office_zip)
                ->get();

            foreach ($office_lookup as $lookup) {

                $lon2 = $lookup->LONGITUDE;
                $lat2 = $lookup->LATITUDE;

                $distance = (3958 * 3.1415926 * sqrt(($lat2 - $lat1) * ($lat2 - $lat1) + cos($lat2 / 57.29578) * cos($lat1 / 57.29578) * ($lon2 - $lon1) * ($lon2 - $lon1)) / 180);

                $round_distance = round($distance, 1);

                // dd($round_distance);

                if ($round_distance < $radius) {
                    $office_lookups[$office_name] = array(
                        "NAME" => $office_name,
                        "BRANCH" => $office_branch,
                        "ADDRESS" => $office_address,
                        "CITY" => $office_city,
                        "STATE" => $office_state,
                        "ZIP" => $office_zip,
                        "PHONE" => $office_phone,
                        "FAX" => $office_fax,
                        "MANAGER" => $office_manager,
                        "ID" => $office_id,
                        "URL" => $office_url,
                        "APP_NAME" => $office_app_name,
                        "APP_URL" => $office_app_url,
                    );
                }

            }
        }

        $offices = $office_lookups;

        return view('pages.find-branch',
            compact(
                'offices',
                'results',
                'searched'
            ));
    }

}
