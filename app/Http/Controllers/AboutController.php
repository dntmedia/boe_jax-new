<?php

namespace App\Http\Controllers;

class AboutController extends Controller
{
    /*
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

// View -----------------------------------------------------------------------

    /*
     * View : partners
     *
     * @return view
     */
    public function partners()
    {
        $pic = rand(1, 5);

        // return view('about.partners');

        return view('about.partners',
            compact(
                'pic'
            ));
    }

    /*
     * View : recognition
     *
     * @return view
     */
    public function recognition()
    {
        // return view('about.recognition');

        return view('about.recognition');

    }

    /*
     * View : who-we-are
     *
     * @return view
     */
    public function whoWeAre()
    {
        return view('about.who-we-are');
    }

}
