<?php

namespace App\Http\Controllers;

use DB;

class BranchController extends Controller
{
    /*
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

// View -----------------------------------------------------------------------

    /*
     * View : recognition
     *
     * @return view
     */
    public function branches()
    {
        return view('branch.branches');
    }

    /*
     * View : our-team
     *
     * @return view
     */
    public function getBranchTeam($id)
    {
        $pathOrUrl = "https://www.boefastapp.net/V2/index.php?token=" . env('API_TOKEN') . "&action=show_team&branch=" . $id;
        $json = file_get_contents($pathOrUrl);
        $officers = json_decode($json, true);

        if ($id == 1114) {
            $branch_address = env('BRANCH_ADDRESS_1114');
        } else {
            $branch_address = env('BRANCH_ADDRESS_1112');
        }

        return view('branch.branch_team',
            compact(
                'branch_address',
                'id',
                'officers'
            ));

    }

    /*
     * View : getOfficer
     *
     * @return view
     */
    public function getOfficer($id)
    {
        $pathOrUrl = "https://www.boefastapp.net/V2/index.php?token=" . env('API_TOKEN') . "&action=show_member&id=" . $id;
        $json = file_get_contents($pathOrUrl);
        $content = json_decode($json, true);

        $officer_id = $content[0]['id'];
        $first_name = $content[0]['fname'];
        $last_name = $content[0]['lname'];
        $job_title = $content[0]['title'];
        $nmls = $content[0]['nmls'];
        $office = $content[0]['phone'];
        $mobile = $content[0]['mobile'];
        $fax = $content[0]['fax'];
        $email = $content[0]['email'];
        $apply_link = '/apply';
        $biography = $content[0]['biography'];
        $photo = $content[0]['photo'];
        $team = $content[0]['team'];
        $team_2 = $content[0]['team_2'];
// dd($team_2);

        $employee_info = DB::select('SELECT * FROM employee_extra where employee_id=:id and active=:active', ['id' => $id, 'active' => 1]);
        $testimonials = DB::select('SELECT * FROM testimonials where employee_id=:id and approved=:approved', ['id' => $id, 'approved' => 'Yes']);
        if (!empty($team)) {
// leader info
            $leaders = explode(',', $team);
            $leader_id = reset($leaders);
            $pathOrUrl = "https://www.boefastapp.net/V2/index.php?token=" . env('API_TOKEN') . "&action=show_member&id=" . $leader_id;
            $json = file_get_contents($pathOrUrl);
            $leader = json_decode($json, true);
            $leader_name = $leader[0]['lname'];
// team info
            $content = call_user_func_array('array_merge', $content);
        }
        if (!empty($team_2)) {
// leader info
            $leaders_2 = explode(',', $team_2);
            $leader_id_2 = reset($leaders_2);
            $pathOrUrl = "https://www.boefastapp.net/V2/index.php?token=" . env('API_TOKEN') . "&action=show_member&id=" . $leader_id_2;
            $json = file_get_contents($pathOrUrl);
            $leader_2 = json_decode($json, true);
            $leader_name_2 = $leader_2[0]['lname'];
// team info
            // $content_2 = $content;
        }
        return view('branch.officer',
            compact(
                'officer_id',
                'first_name',
                'last_name',
                'job_title',
                'nmls',
                'office',
                'mobile',
                'fax',
                'email',
                'apply_link',
                'photo',
                'biography',
                'employee_info',
                'testimonials',
                'leader_name',
                'content',
                'leader_name_2'
            ));
    }

    /*
     * View : our-team
     *
     * @return view
     */
    public function ourTeam()
    {
        $pathOrUrl = "https://www.boefastapp.net/V2/index.php?token=" . env('API_TOKEN') . "&action=show_team&branch=" . env('BRANCH_ID');
        $json = file_get_contents($pathOrUrl);
        $officers = json_decode($json, true);

        if (env('BRANCH_ID_2') != "") {
            $pathOrUrl2 = "https://www.boefastapp.net/V2/index.php?token=" . env('API_TOKEN') . "&action=show_team&branch=" . env('BRANCH_ID_2');
            $json2 = file_get_contents($pathOrUrl2);
            $officers2 = json_decode($json2, true);
        } else {
            $officers2 = null;
        }

        return view('branch.our-team',
            compact(
                'officers',
                'officers2'
            ));

    }

}
